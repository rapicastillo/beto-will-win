from geojson import Feature, FeatureCollection, Point
import csv, json

features = []
row=1
with open('/tmp/gotv_doors.csv', 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for longitude, latitude, num_doors_remaining, num_votes_remaining in reader:
        # print(row, address_id, longitude, latitude, remaining_doors)
        if row==1 or longitude=="" or latitude=="":
            row = row+1
            continue
        row = row+1
        # print(address_id, longitude, latitude, remaining_doors)
        latitude, longitude = map(float, (latitude, longitude))
        features.append(
            Feature(
                geometry = Point((latitude, longitude))
            )
        )
collection = FeatureCollection(features)
with open("/tmp/unknocked_doors.geojson", "w") as f:
    f.write('%s' % collection)

#ogr2ogr -f "GeoJSON" unknocked_doors_proj.geojson unknocked_doors.geojson -s_srs EPSG:3857 -t_srs EPSG:4326
#tippecanoe -o unknocked_doors.mbtiles -Z 13 -z 18 -f -r1 -pk -pf unknocked_doors.geojson
