from boto.s3.connection import S3Connection
from boto.s3.key import Key
import boto
import boto.s3.connection
import requests
import json
import mapbox

service = mapbox.Uploader()

with open("/tmp/betofortexas-targets-new.mbtiles", 'rb') as src:
    upload_resp = service.upload(src, 'middleseatmaps.betofortexas-targets-new')

with open("/tmp/beto-unknocked-doors.mbtiles", 'rb') as doors_src:
    doors_src_resp = service.upload(doors_src, 'middleseatmaps.betofortexas-unknocked-doors')
