from geojson import Feature, FeatureCollection, Point
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import boto
import boto.s3.connection
from dbfread import DBF
import math
import csv
import numpy as np
import json
import gzip
import urllib2
import requests
import os


REGIONS = np.genfromtxt("./data/tx-regions.csv", dtype=None, delimiter=',', names=True, encoding='utf-8')
COUNTIES = np.genfromtxt("./data/counties.csv", dtype=None, delimiter=',', names=True, encoding='utf-8')
DBF_FILE = os.environ.get('DBF_ENDPOINT')


def getRegionById(id):
    if REGIONS[np.where(REGIONS["Id"] == id)][0] is None:
        return None
    return REGIONS[np.where(REGIONS["Id"] == id)][0]

def getCountyById(id):
    if COUNTIES[np.where(COUNTIES["COUNTY_FIPS"] == id)][0] is None:
        return None
    return COUNTIES[np.where(COUNTIES["COUNTY_FIPS"] == id)][0]

def export_data(data):
    filename="precinct_target_geojson_oct30.json"
    if os.path.exists("/tmp/%s" % filename):
        os.remove("/tmp/%s" % filename)
    else: print("The file does not exist")

    with open("/tmp/%s" % filename, 'w') as f:
        f.write(data)

    # aws_host = os.environ.get('AWS_HOST')
    aws_region = os.environ.get('AWS_REGION')
    aws_bucket = os.environ.get('S3_BUCKET')
    cloudfront_id = os.environ.get('CLOUDFRONT_ID')
    access_key_id = os.environ.get('AWS_ACCESS_KEY_ID')
    secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')

    conn = boto.s3.connect_to_region(aws_region,
        aws_access_key_id=access_key_id,
        aws_secret_access_key=secret_access_key,
        is_secure=True,               # uncomment if you are not using ssl
        calling_format = boto.s3.connection.OrdinaryCallingFormat(),
    )

    bucket = conn.get_bucket(aws_bucket)
    key_raw = bucket.get_key("beto-win-map/%s" % filename)

    if key_raw is None:
        print("Creating New Raw File")
        key_raw = bucket.new_key("beto-win-map/%s" % filename)

    print("Uploading RAW to S3")
    key_raw.set_contents_from_filename("/tmp/%s" % filename)
    key_raw.set_acl('public-read')


def old_sync():

    if os.path.exists("/tmp/precinct_data_geojson_raw.geojson"):
        os.remove("/tmp/precinct_data_geojson_raw.geojson")
    else: print("The file does not exist")

    # Download File
    # dbf_file = urllib2.urlopen(DBF_FILE)
    print("Downloading GEOJSON")
    # dbf_file = urllib2.urlopen("https://www.dropbox.com/s/392hmk8v5ykofjm/Precincts_2018_GOTV.geojson?dl=1")
    dbf_file = urllib2.urlopen("https://s3.us-east-2.amazonaws.com/win-dot-beto/Precincts_2018_GOTV.geojson")
    with open('/tmp/precinct_data_geojson_raw.geojson','wb') as output:
      output.write(dbf_file.read())

    print("GEOJSON Downloaded")
    # Process Downloaded File
    RAW = []
    names = ['CNTY', 'PREC', 'PCTKEY', 'WALK_TGTS', 'RMNG_TGTS', 'WLK_DENS']
    formats = ['O','O','O','f', 'f', 'f', 'f', 'f']


    GEOJSON_FILE = "/tmp/precinct_data_geojson_raw.geojson"
    # json_data=.read()
    json_obj = json.load(open(GEOJSON_FILE))


    for datapoint in json_obj['features']:
        prop = datapoint['properties']
        RAW.append(tuple([prop['CNTY'], prop['PREC'], prop['PCTKEY'], \
                              prop['walk_targets'], prop['remaining_targets'], \
                               prop['walk_target_density']]))

    data_type = list(zip(names, formats))
    TARGETS = np.array(RAW, dtype=data_type)

    # compute county targets
    county_targets = []
    end_targets = []
    min_urgency = 100000;
    max_urgency = 0;


    min_urgency = np.amin(TARGETS[np.where(TARGETS["WLK_DENS"] > 0)]['WLK_DENS'])
    max_urgency = np.amax(TARGETS['WLK_DENS'])


    for target in TARGETS:
        county = getCountyById(target['CNTY'])
        county_targets.append({ 'county': county['COUNTY_NAME_CLEAN'], 'fips': county['COUNTY_FIPS'], 'precint_key': target['PCTKEY'],
                                    'target': int(np.nan_to_num(target['WALK_TGTS'])),
                                    'remaining': int(np.nan_to_num(target['RMNG_TGTS'])),
                                    'density': float(np.nan_to_num(target['WLK_DENS'])), 'urgency': '0'})

    # Compute Splits
    lspace_cnt = 5
    lspace = np.logspace(np.log(1), np.log(10000), num=lspace_cnt, endpoint=True, base=np.e)
    splits = [lspace[lspace_cnt-5], lspace[lspace_cnt-4], lspace[lspace_cnt-3], lspace[lspace_cnt-2], (lspace[lspace_cnt-1] + lspace[lspace_cnt-2])/3]

    split_len = len(splits)
    # Set urgencies...
    for target in county_targets:
        if target['density'] > splits[split_len-1]:
            target['urgency'] = 'l1'
        elif target['density'] > splits[split_len-2]:
            target['urgency'] = 'l2'
        elif target['density'] > splits[split_len-3]:
            target['urgency'] = 'l3'
        elif target['density'] > splits[split_len-4]:
            target['urgency'] = 'l4'
        elif target['density'] > splits[split_len-5]:
            target['urgency'] = 'l5'

    final_targets = county_targets
    county_targets = []
    for county in COUNTIES:
        ed_in_region = TARGETS[np.where(TARGETS["CNTY"] == county["COUNTY_FIPS"])]
        total_targets = np.nansum(ed_in_region['WALK_TGTS'])
        remaining_targets = np.nansum(ed_in_region['RMNG_TGTS'])
        county_targets.append({ 'name': county['COUNTY_NAME_CLEAN'], 'fips': county['COUNTY_FIPS'], 'target': int(total_targets), 'remaining': int(remaining_targets)})

    output = {'counties': county_targets, 'precints': final_targets, 'splits': splits}
    raw_data = json.dumps(output)

    ## Print Raw Data
    export_data(raw_data)

def sync():
    GEOJSON_FILE = "/tmp/precinct_data_geojson_raw_new.geojson"
    if os.path.exists(GEOJSON_FILE):
        os.remove(GEOJSON_FILE)
    else: print("The file does not exist")

    # Download File
    # dbf_file = urllib2.urlopen(DBF_FILE)
    print("Downloading GEOJSON")
    # dbf_file = urllib2.urlopen("https://www.dropbox.com/s/392hmk8v5ykofjm/Precincts_2018_GOTV.geojson?dl=1")
    dbf_file = urllib2.urlopen("https://s3.us-east-2.amazonaws.com/win-dot-beto/Precincts_2018_GOTV_new.geojson")
    with open(GEOJSON_FILE,'wb') as output:
      output.write(dbf_file.read())

    print("GEOJSON Downloaded")
    # Process Downloaded File
    RAW = []
    names = ['CNTY', 'PCTKEY', 'WALK_TGTS', 'WALK_REMAINING', 'WALK_CMPLT', 'PRIORITY', 'TTL_KNOCKS', 'TTL_CALLS', 'TTL_VOTEPLAN', 'TTL_VOTED']
    formats = ['O', 'O','f', 'f', 'f', 'f', 'f', 'f', 'f', 'f']



    # json_data=.read()
    json_obj = json.load(open(GEOJSON_FILE))


    for datapoint in json_obj['features']:
        prop = datapoint['properties']
        RAW.append(tuple([prop['CNTY'], prop['PCTKEY'],
                              prop['gotv_universe'], prop['not_walked_not_voted'],
                              prop['gotv_universe_walked'],
                               prop['precinct_priority_int'],
                              prop['total_knocks'], prop['total_calls'], prop['total_vote_plans'],
                              prop['gotv_universe_voted']]))

    data_type = list(zip(names, formats))
    TARGETS = np.array(RAW, dtype=data_type)

    # compute county targets
    # cnty = county
    # key = precint key
    # tg = univ targets
    # cp = univ targets completed
    # pr = priority
    # kn = knocks
    # cl = calls
    # vp = voteplan
    # vtd = voted

    county_targets = []
    for target in TARGETS:
        county = getCountyById(target['CNTY'])
        county_targets.append({ 'cnty': county['COUNTY_NAME_CLEAN'], 'fips': county['COUNTY_FIPS'],
                                   'key': target['PCTKEY'],
                                    'tg': int(np.nan_to_num(target['WALK_TGTS'])),
                                    'rm': int(np.nan_to_num(target['WALK_REMAINING'])),
                                    'cp': int(np.nan_to_num(target['WALK_CMPLT'])),
                                    'pr': int(target['PRIORITY']),
                                    'kn': int(np.nan_to_num(target['TTL_KNOCKS'])),
                                     'cl': int(np.nan_to_num(target['TTL_CALLS'])),
                                     'vp': int(np.nan_to_num(target['TTL_VOTEPLAN'])),
                                     'vtd': int(np.nan_to_num(target['TTL_VOTED']))
                                    })

    final_targets = county_targets

    county_targets = []
    for county in COUNTIES:
        ed_in_region = TARGETS[np.where(TARGETS["CNTY"] == county["COUNTY_FIPS"])]
        total_targets = np.nansum(ed_in_region['WALK_TGTS'])
        total_univ_completed = np.nansum(ed_in_region['WALK_CMPLT'])
        total_remaining = np.nansum(ed_in_region['WALK_REMAINING'])
        total_knocks = np.nansum(ed_in_region['TTL_KNOCKS'])
        total_calls = np.nansum(ed_in_region['TTL_CALLS'])
        total_voteplan = np.nansum(ed_in_region['TTL_VOTEPLAN'])
        total_voted = np.nansum(ed_in_region['TTL_VOTED'])

        county_targets.append({ 'name': county['COUNTY_NAME_CLEAN'], 'fips': county['COUNTY_FIPS'],
                                'tg': int(total_targets),
                                'rm': int(total_remaining),
                                'cp': int(total_univ_completed),
                                'kn': int(total_knocks),
                                'cl': int(total_calls),
                                'vp': int(total_voteplan),
                                'vtd': int(total_voted) })

    overall_univ_targets = int(np.nansum(TARGETS['WALK_TGTS']))
    overall_univ_completed = int(np.nansum(TARGETS['WALK_CMPLT']))
    overall_remaining = int(np.nansum(TARGETS['WALK_REMAINING']))
    overall_knocks = int(np.nansum(TARGETS['TTL_KNOCKS']))
    overall_calls = int(np.nansum(TARGETS['TTL_CALLS']))
    overall_voteplan = int(np.nansum(TARGETS['TTL_VOTEPLAN']))
    overall_voted = int(np.nansum(TARGETS['TTL_VOTED']))

    totals = {
        'tg': overall_univ_targets,
        'rm': overall_remaining,
        'cp': overall_univ_completed,
        'kn': overall_knocks,
        'cl': overall_calls,
        'vp': overall_voteplan,
        'vtd': overall_voted
    }


    output = {'counties': county_targets, 'precints': final_targets, 'totals': totals }
    raw_data = json.dumps(output)

    ## Print Raw Data
    export_data(raw_data)


def pull_unknocked_doors():
    print("Building Unknocked Doors")
    features = []
    row=1

    CSV_FILE = "/tmp/gotv_doors.csv"
    dbf_file = urllib2.urlopen("https://s3.us-east-2.amazonaws.com/win-dot-beto/gotv_doors.csv")
    with open(CSV_FILE,'wb') as output:
      output.write(dbf_file.read())


    with open(CSV_FILE, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for longitude, latitude, num_doors_remaining, num_votes_remaining in reader:
            # print(row, address_id, longitude, latitude, remaining_doors)
            if row==1 or longitude=="" or latitude=="":
                row = row+1
                continue
            row = row+1
            # print(address_id, longitude, latitude, remaining_doors)
            latitude, longitude = map(float, (latitude, longitude))
            features.append(
                Feature(
                    geometry = Point((latitude, longitude))
                )
            )
    collection = FeatureCollection(features)
    with open("/tmp/unknocked_doors.geojson", "w") as f:
        f.write('%s' % collection)
    print("Unknocked doors Finished")

sync()
pull_unknocked_doors()
