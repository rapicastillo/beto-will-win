

export default {
  push: (params) => {
    const state = $.deparam(window.location.search.substring(1));
    const newState = Object.assign(state, params);
    const controls = Object.assign(newState, $.deparam($("#map-controls-form").serialize()));
    window.history.pushState(state, null, `/?${$.param(newState)}`);
    $(window).trigger('window.applyStates', newState);
  }
};
