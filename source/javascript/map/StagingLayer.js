import HistoryHandler from "../history/HistoryHandler";

export default class StagingLayer {
  constructor(geojsonLayer, data) {
    this.geojsonLayer = geojsonLayer;
    this.data = data;

    this.layer = L.markerClusterGroup({
      spiderfyDistanceMultiplier: 1,
      iconCreateFunction: function(cluster) {
        const remaining = cluster.getAllChildMarkers().map((item) => item.options.targets.remaining).reduce((total, num)=>total+num);
        const target = cluster.getAllChildMarkers().map((item) => item.options.targets.target).reduce((total, num)=>total+num);

        return L.divIcon({
          html: `<div><span>${target-remaining}/${target}</span></div>`,
          className: "marker-cluster ",
        });
      }
    });

    this.render();
  }

  render() {
    let _this = this;
    this.data.forEach(function(item) {
      if (item.remaining == item.target) {
        return;
      }

      var layer = _this.geojsonLayer.getLayer(item.fips);
      var m = L.marker(layer.getCenter(), {
          targets: item,
          icon: new L.DivIcon({
              className: 'my-div-icon',
              html: `<div class="icon-container"><img class="my-div-image" src="/img/marker.png"/>
                    <span class="my-div-span">${item.target - item.remaining}/${item.target}</span></div>`
          })
      });

      m.bindPopup(`<div>${item.remaining} Volunteer HQ's needed to set <br/>up in ${layer.feature.properties.COUNTY}</div><br/><div><strong>Click to start a staging site</strong></div>`,
              {offset: [-4, -20]});

      m.on('dblclick', (e) => {
        const center = layer.getCenter();
        HistoryHandler.push({ lat: center.lat, lng: center.lng, fips: layer.feature.targets.fips, region: layer.feature.targets.region });
      });

      m._leaflet_id = "marker" + item.fips;
      // m.addTo(stagingLayer);
      _this.layer.addLayer(m);
    });
  }
}
