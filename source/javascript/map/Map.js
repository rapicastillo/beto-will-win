import HistoryHandler from "../history/HistoryHandler";
import StagingLayer from './StagingLayer';
import helper from '../helper';

export const TEXAS_BOUNDS = [[-106.65,25.84],[-93.51,36.5]];

export default class Map {

  constructor(container, /*countiesGeoJson,*/ targets, actions, centroids, source) {
    this.container = container;
    this.sourceUrl = source;
    // this.countiesGeoJson = countiesGeoJson;
    this.targets = targets;
    this.actionData = actions;
    this.eventCategories = actions.categories;
    this.centroids = centroids;

    // This is initially blank, but will be updated as users hover
    // This is the precinct hover
    this.hoveredData = {
        "type": "Feature",
        "geometry": {
          "type": "Polygon",
          "coordinates": [[0,0]]
        }
    };

    // This is initially blank but will be painted on the map
    // as the user interacts (this is the green dot.)
    this.pointedData = {
        "type": "Feature",
        "geometry": {
          "type": "Point",
          "coordinates": [0,0]
        }
    };

    this.focusedPrecinct = (lng = null, lat = null) => ({
        "type": "FeatureCollection",
        "features": [{
          "type": "Feature",
          "properties": {},
          "geometry": {
            "type": "Point",
            "coordinates": [parseFloat(lng), parseFloat(lat)],
          }
        }]
      });

    this.popup = new mapboxgl.Popup({
      closeOnClick: false
    });

    this.popup = new mapboxgl.Popup({
      closeOnClick: false
    });

    this.defaultZoom = 11;
    this.hoveredTimeout = null;
    mapboxgl.accessToken = 'pk.eyJ1IjoibWlkZGxlc2VhdG1hcHMiLCJhIjoiY2pjcXEzNjF4MDRndzMzbTdiZW4wd3ZoMyJ9.xXm1UTWswMqQoXcSjgxV_Q';
    this.map = new mapboxgl.Map({
      container: this.container,
      // style: '',
      // style: 'mapbox://styles/middleseatmaps/cjnch0d1f6v2s2tqw1c9fl5ns?fresh=true',
      minZoom: 5,
      maxZoom: 20,
      doubleClickZoom: false,
      center: [ -99.80986751078535, 31.544043826594077],
      zoom: 6,
      maxTileCacheSize: 0
    });

    //mapbox://styles/middleseatmaps/cjnyq87ws0yor2sqgu8bfptlc
    //mapbox://styles/middleseatmaps/cjnuwmkhk56822roe39cvt1e4
    this.map.setStyle("mapbox://styles/middleseatmaps/cjnyo1vmv8ocr2spb05lkkwjj?fresh=true");

    // Force destroy cache

    this.map.addControl(new mapboxgl.NavigationControl());

    // this.map.fitBounds(TEXAS_BOUNDS, {duration: 0, animate: false});
    this.map.on('style.load', this.activateMap.bind(this));

    // () => {
    //   _this.map.getLayer
    //   // _this.renderGeojsonLayer();
    //   // _this.renderEvents();
    //   // _this.renderHQs();
    //   // _this.initializeEvents();
    // }
  }

  activateMap() {

    // Force clear cache

    // let style = this.map.getStyle();
    // style.sources['composite'].url = style.sources['composite'].url +`?fresh=true&rand=${Math.random()}`;
    // style.sources['composite']['dirty'] = Math.random();
    // style.sources['mySource'].dirty = Math.random();

    this.addHoverLayer();
    this.renderHQs();
    this.renderPopups();
    this.renderEvents();
    this.initializeEvents();

    this.choroplethPriority();
    this.choroplethCompleteness();

    $(document).trigger("map.style.loaded");
  }

  choroplethPriority() {
    // alert("Choropleth Urgency");

    // const splits = this.targets.splits;
    // const len = splits.length;
    const priority_expr = ['get', 'precinct_priority_int'];
    const expr = ['case',
        ["==", priority_expr, "5"], 'rgb(144, 207, 231)',
        ["==", priority_expr, "4"], 'rgb(172, 164, 200)',
        ["==", priority_expr, "3"], 'rgb(179, 124, 153)',
        ["==", priority_expr, "2"], 'rgb(160, 93, 103)',
        ["==", priority_expr, "1"], 'rgb(127, 69, 60)',
        'rgba(0,0,0,0)'
    ];

    const opacity_expr = ['case',
        ["==", priority_expr, "5"], 0.2,
        ["==", priority_expr, "4"], 0.5,
        ["==", priority_expr, "3"], 0.5,
        ["==", priority_expr, "2"], 0.8,
        ["==", priority_expr, "1"], 0.8,
        0
    ];

    this.map.setPaintProperty('precincts-priority-x', 'fill-color', expr);
    this.map.setPaintProperty('precincts-priority-x', 'fill-opacity', opacity_expr);

  }

  choroplethCompleteness() {
    const complete_expr = ['/',
                            ['-',
                              ['get', 'gotv_universe'],
                              ['get', 'not_walked_not_voted'],
                            ],
                            ['get', 'gotv_universe']
                          ];
    // const complete_expr = ['/',
    //                           ['+',
    //                             ['get', 'gotv_universe_voted'],
    //                             ['get', 'gotv_universe_walked']
    //                           ],
    //                           ['get', 'gotv_universe']
    //                       ];
    const expr = [
      'case',
      ['==', ['get', 'gotv_universe'], "0"], 'rgba(0,0,0,0)',
      ['==', ['get', 'not_walked_not_voted'], "0"], 'rgba(0,0,0,0)',
      ['==', ['get', 'not_walked_not_voted'], ['get', 'gotv_universe']], 'rgba(0,0,0,0)',
      ['<=', complete_expr, 0], 'rgba(0,0,0,0)',
      ['>=', complete_expr, 1], 'rgba(0, 149, 235, 1)',
      ['all', ['<', complete_expr, 1], ['>', complete_expr, 0]],  ['step',
            complete_expr,
            'rgba(255, 0, 170, 0.1)',
            0.2, 'rgba(204, 51, 170, 0.3)',
            0.4, 'rgba(153, 153, 170, 0.5)',
            0.6, 'rgba(102, 204, 170, 0.7)',
            0.8, 'rgba(51, 255, 170, 0.9)'
        ],
      'rgba(0,0,0,0)'
    ];

    const opa_expr = [
      'case',
      ['==', ['get', 'gotv_universe'], "0"], 0,
      ['<=', complete_expr, 0], 0,
      0.7
    ];

    this.map.setPaintProperty('precinct-completeness', 'fill-color', expr);
    this.map.setPaintProperty('precinct-completeness', 'fill-opacity', opa_expr);

  }


  addHoverLayer() {
    this.map.addSource('hovered', { type: 'geojson', data: this.hoveredData });
    this.map.addLayer({
      id: 'hovered',
      type: 'fill',
      source: 'hovered',
      "paint": {
          "fill-color": "#000000",
          "fill-opacity": 0.05
      }
    }, 'precinct-completeness');

    this.map.addSource('pointed', { type: 'geojson', data: this.pointedData});
    this.map.addLayer({
      id: 'pointed',
      type: 'circle',
      source: 'pointed',
      paint: {
        "circle-radius": 4,
        "circle-color": "#bdff00",
        "circle-opacity": 1, //['get', 'opacity'],
        "circle-stroke-width": 1,
        "circle-stroke-color": "white",
        "circle-stroke-opacity": 1
      }
    });
  }

  getPrecintCoords(precint_key) {
    const res = this.centroids.filter(item => item.PCTKEY == precint_key)[0];
    return res ? {lat: res.latitude, lng: res.longitude} : null;
  }

  getBounds(coordinates) {
    return coordinates.reduce(function(bounds, coord) {
        return bounds.extend(coord);
    }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
  }

  getRegionInfo(regionId) {
    return this.regions.filter((i) => i.id == regionId)[0];
  }

  initializeEvents() {

    $("form#map-controls-form input").on("change", () => {
      const params = $.deparam($("form#map-controls-form").serialize());
      let values = Object.assign({'show_popups': false, 'show_hq': false, 'show_events': false, 'ctrl-road': false, 'show_strategy': 'targets'}, params);
      values.action = 'map-change'
      HistoryHandler.push(values);
    });

    $("#map-insight-container h3, #map-insight-container a.cta-area, #map-insight-container div.cty-information").on("tap", (e) => {
      const pctkey = $(e.currentTarget).data("precint");
      this.setPoint(pctkey);
      var center = this.getPrecintCoords(pctkey) || {};
      HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: pctkey });
      $("a#show-more-btn").trigger("click");
      this.hideInfo();
    });

    const show_layer_info = (event) => {
      const e = event;

      const prop = e.features[0].properties;
      this.hoveredData.geometry.coordinates = e.features[0].geometry.coordinates
      clearTimeout(this.hoveredTimeout);
      this.map.getSource('hovered').setData(this.hoveredData);
      this.showLayerInfo(this.getTargetData(prop.PCTKEY));
    };
    //mouseover layer
    this.map.on('mousemove', "precincts-priority-x", show_layer_info);
    this.map.on('mousemove', "precinct-completeness", show_layer_info);


    this.map.on('mouseout', (event) => {
      this.hoveredData.geometry.coordinates = [[]];
      this.map.getSource('hovered').setData(this.hoveredData);
      if ($(window).width() >= 850) {
        this.hideInfo();
      }
    })

    this.map.on('mouseenter', "precincts-priority-x", (e) =>{
      this.map.getCanvas().style.cursor = 'pointer';
    });
    this.map.on('mouseleave', "precincts-priority-x", (e) => {
      this.map.getCanvas().style.cursor = '';

      // Only do this on desktop
      if ($(window).width() >= 850) {
        this.hideInfo();
      }
    })

   //  this.map.on("mousemove", "hover-layer", function(e) {
   //     if (e.features.length > 0) {
   //         if (this.hovered_id) {
   //             this.map.setFeatureState({source: 'composite', id: this.hovered_id}, { hover: false});
   //         }
   //         this.hovered_id = e.features[0].PCTKEY;
   //         this.map.setFeatureState({source: 'composite', sourceLayer: 'precints', PCTKEY: this.hovered_id}, { hover: true});
   //     }
   // });
    const dblclick_event = (e)=> {
      const pctkey = e.features[0].properties.PCTKEY;
      this.setPoint(pctkey);
      var center = this.getPrecintCoords(pctkey) || {};
      HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: pctkey });
    };
    this.map.on('dblclick', "precincts-priority-x", dblclick_event);
    this.map.on('dblclick', "precinct-completeness", dblclick_event);

    //clickon event
    this.map.on("click", 'events', (e) => {
      var coordinates = e.features[0].geometry.coordinates.slice();
      var description = e.features[0].properties.description;

      this.popup.setLngLat(coordinates)
            .setHTML(description)
            .addTo(this.map)
    });

    this.map.on("click", 'popups', (e) => {
      var coordinates = e.features[0].geometry.coordinates.slice();
      var description = e.features[0].properties.description;

      this.popup.setLngLat(coordinates)
            .setHTML(description)
            .addTo(this.map)
    });

    //clickon marker
    this.map.on("click", 'headquarters', (e) => {
      var coordinates = e.features[0].geometry.coordinates.slice();
      var description = e.features[0].properties.description;

      this.popup.setLngLat(coordinates)
            .setHTML(description)
            .addTo(this.map)
    });
  }

  getUrgencyScore(priority) {
    // const len = this.targets.splits.length;
    let priorityLevel = priority;
    // switch (priority) {
    //   case "5) Mid": priorityLevel = 5; break;
    //   case '4) High-Mid': priorityLevel = 4; break;
    //   case '3) High': priorityLevel = 3; break;
    //   case '2) Very High': priorityLevel = 2; break;
    //   case '1) Top': priorityLevel = 1; break;
    // }
    if (priorityLevel > 0) {
      return `<i class='urgency' data-level='l${priorityLevel}'></i>`;
    }
    return "<i class='no-urgency'></i>";
  }


  showLayerInfo(data) {
    // Populate About information
    const _percFormat = d3.format(".1%");
    const _format = (num)  => num < 10000 ? d3.format(",")(num) : d3.format(".2s")(num);

    $("#map-insight").show(0);
    const coords = this.getPrecintCoords(data.key);

    // PRecint is the focus
    if (!!data.fips & !!data.key ) {
      let percComplete = 1 - (parseFloat(data.rm) / parseFloat(data.tg));
          percComplete = percComplete > 1 ? 1 : percComplete;
      let precintId = data.key.substring((data.fips+"").length);
      const completed = data.cp;
      $("#map-insight-container").attr('data-precint',`${data.precint_key}`).html(
        ` <h3>
            <strong>${this.getUrgencyScore(data.pr)} ${data.cnty} County</strong>
            <span>Precinct # ${precintId}</span>
          </h3>
          <div class='cty-information'>
            <div class='tbl-container'>
              <table class='table table-sm table-desktop'>
                <tr><th scope="col">GOTV Universe</th><td>${_format(data.tg)}</td></tr>
                <tr><th scope="col">Doors Knocked</th><td>${_format(completed)}</td></tr>
                <tr><th scope="col">Early Voter Doors</th><td>${_format(data.vtd)}</td></tr>
                <tr><th scope="col">Remaining Doors</th><td>${_format(data.rm)}</td></tr>
                <tr class='perc-complete' data-level='L${percComplete > 0 ? Math.floor(percComplete * 5) * 20 : ''}'><th scope="col">% Complete</th><td>${_percFormat(percComplete)}</td></tr>
                <tr class='other-stat-row'><th scope="col">Total Doors Knocked</th><td>${_format(data.kn)}</td></tr>
                <tr class='other-stat-row'><th scope="col">Total Calls Made</th><td>${_format(data.cl)}</td></tr>
                <tr class='other-stat-row'><th scope="col">Vote Plans Made</th><td>${_format(data.vp)}</td></tr>
              </table>
              <table class='table table-sm table-mobile'>
                <tr>
                  <td>
                    <div class='others-tbl-area'>
                      <span class='other-lbl'>
                        <strong>Univ Doors Target</strong>
                        <span>${_format(data.tg)}</span>
                      </span>
                      <span class='other-lbl'>
                        <strong>Univ Doors Knocked</strong>
                        <span>${_format(completed)}</span>
                      </span>
                      <span class='other-lbl'>
                        <strong>Early Voters</strong>
                        <span>${_format(data.vtd)}</span>
                      </span>
                      <span class='other-lbl'>
                        <strong>Remaining Doors</strong>
                        <span>${_format(data.rm)}</span>
                      </span>
                      <span class='other-lbl'>
                        <strong>% Complete</strong>
                        <span>${_percFormat(percComplete)}</span>
                      </span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class='others-tbl-area'>
                      <span class='other-lbl'>
                        <strong>Total Doors Knocked</strong>
                        <span>${_format(data.kn)}</span>
                      </span>
                      <span class='other-lbl'>
                        <strong>Total Calls Made</strong>
                        <span>${_format(data.cl)}</span>
                      </span>
                      <span class='other-lbl'>
                        <strong>Vote Plans Made</strong>
                        <span>${_format(data.vp)}</span>
                      </span>
                    </div>
                  </td>
                </tr>
              </table>
              ${this.getUrgencyScore(data.pr)}
            </div>
            <a class='cta-area' ></a>
            <a class='cta-directions' href='https://www.google.com/maps/dir//${coords.lat},${coords.lng}/@${coords.lat},${coords.lng},12z' target='_blank'><i class='fa fa-map-marker'></i>
              <span>Get Directions</span>
            </a>
          </div>
        `
      );
    }
  }

  hideInfo() {
    $("#map-insight").hide(0);
  }

  getTargetData(precint_key) {
    return this.targets.precints.filter(t=>t.key == precint_key)[0];
  }

  showPoint(precint_key) {
    const { lat, lng } = this.getPrecintCoords(precint_key);
    this.pointedData.geometry.coordinates = [lng, lat];
    this.map.getSource('pointed').setData(this.pointedData);
  }

  setPoint(precint_key) {
    const { lat, lng } = this.getPrecintCoords(precint_key);
    if (this.map.getSource('focusedPrecinct') !== undefined) {
      this.map.getSource('focusedPrecinct').setData(this.focusedPrecinct(parseFloat(lng), parseFloat(lat)));
    }
  }

  hidePoint() {
    this.pointedData.geometry.coordinates = [];
    this.map.getSource('pointed').setData(this.pointedData);
  }

  renderEvents() {
    const _this = this;

        const geojson = {
          "type": "FeatureCollection",
          "features": _this.actionData.events
                        .filter(item=> !["popups", "grassroots-hq", "campaign-office"].includes(item.campaign))
                        .map(item => (
                          {
                            "type": "Feature",
                            "properties": {
                              "id": `${item.lng}-${item.lat}`,
                              "categories": item.categories,
                              "lat": item.lat,
                              "lng": item.lng,
                              "description":  _this.renderEventItem(item, this.sourceUrl)
                            },
                            "geometry": {
                              "type": "Point",
                              "coordinates": [item.lng, item.lat]
                            }
                          })
                        )
        };

        // item.categories == "blockwalk";
      _this.map.addLayer({
        "id": "events",
        "type": "circle",
        "source": {
          "type": "geojson",
          "data": geojson
        },
        "paint": {
          "circle-radius": 3,
          "circle-color": "#555",
          "circle-opacity": 0.6,
          "circle-stroke-width": 2,
          "circle-stroke-color": "white",
          "circle-stroke-opacity": 1
        }
      }, "popups");

      // Add layer map
      _this.map.loadImage('/img/star-green.png', (error, greenPulse) => {
        _this.map.addImage("greenpulse", greenPulse);
        this.map.addSource('focusedPrecinct',  {
          type: 'geojson',
          data: this.focusedPrecinct() });

        _this.map.addLayer({
          "id": "focusedPrecinct",
          "type": "symbol",
          "source": 'focusedPrecinct',
          "layout": {
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
            'text-allow-overlap': true,
            "icon-image": "greenpulse",
            "icon-size": 1,
            "icon-offset": [0, -15]
            }
        });

      });

      // ['case',
      //                     ['==', ['get', 'categories'], 'blockwalk'],
      //                     5,
      //                     3
      //                 ]
    /* Render Events */
    // const info = this.actionData.events.filter(item => item.campaign === "action");
    // // const compressMap = info.reduce({}, {})
    // const _this = this;
    // const markers = info.map(item=>{
    //   const isBlockWalk =   item.categories == "blockwalk";
    //   return L.circleMarker([item.lat, item.lng], {
    //       radius: isBlockWalk ? 5 : 3,
    //       color: '#FFF',
    //       weight: 1,
    //       fillColor: isBlockWalk ? "#ff3251" : '#555',
    //       fillOpacity: 0.6
    //   }).bindPopup(_this.renderEventItem(item));
    // });
    //
    //
    // this.eventsLayer = L.layerGroup(markers);
    //
    // this.eventsLayer.addTo(this.map);

  }
  renderPopups() {
    const _this = this;
    const data = this.actionData.events.filter(item=>item.is_popup);
    let b = data.map(i=>i.campaign);

    const geojson = {
      "type": "FeatureCollection",
      "features": data
                    .map(item => (
                      {
                        "type": "Feature",
                        "properties": {
                          "id": `${item.lng}-${item.lat}`,
                          "icon": item.categories,
                          "lng": item.lng,
                          "lat": item.lat,
                          "description":  _this.renderStagingItem(item, this.sourceUrl)
                        },
                        "geometry": {
                          "type": "Point",
                          "coordinates": [item.lng, item.lat]
                        }
                      })
                    )
    };

    _this.map.addLayer({
      "id": "popups",
      "type": "circle",
      "source": {
        "type": "geojson",
        "data": geojson
      },
      "paint": {
        "circle-radius": 5,
        "circle-color": "#ff3251",
        "circle-opacity": 0.6,
        "circle-stroke-width": 2,
        "circle-stroke-color": "white",
        "circle-stroke-opacity": 1
      }
    });
  }

  renderHQs() {
    const _this = this;
    let waiter = null, loadDone = false;
    _this.map.loadImage('/img/star-blue.png', (error, starBlue) => {
      _this.map.loadImage('/img/star-black.png', (error, starBlack) => {

        _this.map.addImage("grassrootshq", starBlack);
        _this.map.addImage("officialhq", starBlue);

        const geojson = {
          "type": "FeatureCollection",
          "features": _this.actionData.events
                        .filter(item=>item.is_office||item.is_grassroots_office)
                        .map(item => (
                          {
                            "type": "Feature",
                            "properties": {
                              "id": `${item.lng}-${item.lat}`,
                              "icon": item.is_office ? 'officialhq' : 'grassrootshq',
                              "lng": item.lng,
                              "lat": item.lat,
                              "description":  _this.renderStagingItem(item, this.sourceUrl)
                            },
                            "geometry": {
                              "type": "Point",
                              "coordinates": [item.lng, item.lat]
                            }
                          })
                        )
        };

        _this.map.addLayer({
          "id": "headquarters",
          "type": "symbol",
          "source": {
            "type": "geojson",
            "data": geojson
          },
          "layout": {
            'icon-allow-overlap': true,
            'icon-ignore-placement': true,
            'text-ignore-placement': true,
            'text-allow-overlap': true,
              "icon-image": "{icon}",
              "icon-size": 1,
              "icon-offset": [0, -15]
            }
        });


        // Hack -- Check if map is hiding HQs in the beginning
        const o = helper.getQueryString();

        if (o.show_hq !== undefined) {
          this.map.setLayoutProperty('headquarters', 'visibility', o.show_hq.toString() === "true" ? 'visible' : 'none');
        }
      })
    });

    // const info = this.actionData.events.filter(item => ["grassroots-hq", "campaign-office"].includes(item.campaign));
    // const grassrootsIcon = L.icon({
    //   iconUrl: '/img/star-black.png',
    //   iconSize: [30, 30],
    //   iconAnchor: [15, 30]
    // });
    //
    // const officialIcon = L.icon({
    //   iconUrl: '/img/star-green.png',
    //   iconSize: [30, 30],
    //   iconAnchor: [15, 30]
    // });
    //
    // const _this = this;
    // const markers = info.map(item=>{
    //
    //   return L.marker([item.lat, item.lng], {
    //     icon: item.campaign === "grassroots-hq" ? grassrootsIcon : officialIcon
    //   }).bindPopup(_this.renderStagingItem(item));
    // });
    //
    // this.headquartersLayer = L.layerGroup(markers);
    //
    // this.headquartersLayer.addTo(this.map);
  }

  renderEventItem(event, source) {
    const categories = event.categories.split(',');
    return `
      <div class='event-detail-container'>
        <h5 class='event-categ'>${categories.length == 1 ? categories[0] : categories.join(" | ")}</h5>
        <h4><a target='_blank' href='${event.url}?source=${source}'>${event.title}</a></h4>
        <div class='event-details'>
          <div class='event-dateplace'>
            <p class='event-date'>${moment(`${event.start_datetime}`).format("MMM D, YYYY • HH:mm a")}</p>
            <p class='event-venue'>${event.venue}</p>
          </div>
          <div class='event-rsvp'>
            <a href='${event.url}?source=${source}' target='_blank' class='beto-cta'>RSVP</a>
          </div>
        </div>
      </div>
    `;
  }

  renderStagingItem(staging, source) {
    const categories = staging.categories.split(',');
    return `
      <div class='staging-detail-container'>
        <h5 class='staging-categ'>${staging.is_office ? "Official HQ" : staging.is_popup ? 'Pop-up Office' : 'Grassroots HQ'}</h5>
        <h4><a target='_blank' href='${staging.url}?source=${source}'>${staging.title}</a></h4>
        <div class='staging-details'>
          <div class='staging-dateplace'>
            <p class='staging-description'>${staging.venue}</p>
            <div class='staging-description'>
              ${!!staging.event_popuphours ? staging.event_popuphours.split(';').map(i=>`<div>${i}</div>`).join("") : ""}
              <div>${!!staging.event_popupcontacts ? staging.event_popupcontacts : ''}</div>
            </div>
          </div>
          <div class='staging-rsvp'>
            <a href='${staging.url}?source=${source}' target='_blank' class='beto-cta'>VOLUNTEER</a>
          </div>
        </div>
      </div>
    `;
  }


  refreshMap(o) {

    o.show_popups!==undefined && this.map.setLayoutProperty('popups', 'visibility', o.show_popups.toString() === "true" ? 'visible' : 'none');
    o.show_events!==undefined && this.map.setLayoutProperty('events', 'visibility', o.show_events.toString() === "true"? 'visible' : 'none');


    if (o['ctrl-road'] !== undefined) {
      if (o['ctrl-road'] == "true") {
        this.map.moveLayer('precinct-completeness', 'building');
        this.map.moveLayer('precincts-priority-x', 'building');
      } else {
        this.map.moveLayer('precinct-completeness', "country-label-lg");
        this.map.moveLayer('precincts-priority-x', "country-label-lg");
      }
    }

    if (o.show_strategy===undefined || o.show_strategy == 'targets') {
      this.map.setLayoutProperty('precinct-completeness', 'visibility', 'none');
      this.map.setLayoutProperty('precincts-priority-x', 'visibility',  'visible');
      $("#map-legend-container").attr("data-visible", "targets");
    } else {
      $("#map-legend-container").attr("data-visible", "completed");
      this.map.setLayoutProperty('precinct-completeness', 'visibility', 'visible');
      this.map.setLayoutProperty('precincts-priority-x', 'visibility',  'none');
    }

    try {
      if (this.map.getStyle().layers.filter(i=>i.id=="headquarters").length) {
        o.show_hq !== undefined && this.map.setLayoutProperty('headquarters', 'visibility', o.show_hq.toString() === "true"? 'visible' : 'none');
      }

    } catch(e) {}

    // this.headquartersLayer[o.show_hq=="true" ? 'addTo' : 'removeFrom'](this.map);
    // this.eventsLayer[o.show_events=="true" ? 'addTo' : 'removeFrom'](this.map);
    // this.geojsonLayer[o.show_precints=="true" ? 'addTo' : 'removeFrom'](this.map);

    switch(o.action) {
      case "precinct-search":
        this.setPoint(o.precint_key)
      case "zipcode-search":
      case "precint-click":
        if(o.lat && o.lng) {
          this.map.easeTo({center: new mapboxgl.LngLat(o.lng, o.lat), zoom: this.map.getZoom() < 12 ? 12 : this.map.getZoom() })
        }
      break;
    }
  }
}
