import { Map } from "./map";
import { BarGraph, Zipcode, Events, Precinct, StagingSites } from "./action";
import { TailoredInfo } from "./information";
import { getQueryString } from "./helper";

const loadingBar = new ldBar("#beto-loader");
      loadingBar.set(0);

// Load all data
const queryString = getQueryString();

let MapManager,
    ZipcodeManager,
    DoorsGraphManager,
    PrecinctManager,
    EventsManager,
    StagingSiteManager,
    tailoredInfoManager;

document.addEventListener("DOMContentLoaded",() => loadData());


const loadData = () => {
  loadingBar.set(10);
  d3.csv('//d1y0otadi3knf6.cloudfront.net/d/us_postal_codes.gz', (zipcode) => {
    loadingBar.set(60);
    $.when(
      $.getJSON("/data/precinct_target_geojson_oct30.json"),
      // $.getJSON("/data/derived/targets.json"),
      $.getJSON("https://d1lojfn0ru84zx.cloudfront.net/BTnorWqhLwDyGhVKm0BCiN6MK1UbD9Lq.json")
    )
    .done(
      (targets, actions) => {

      d3.csv('https://d1lojfn0ru84zx.cloudfront.net/beto-win-map/centroids.csv', (centroid) => {
        window.TARGETS = targets;
          loadingBar.set(100);

          // Made this async for graceful map loading..
          setTimeout(() => {
            const source = queryString.source || "win";
            window.mapManager = MapManager = new Map('map', targets[0], actions[0], centroid, source);
            DoorsGraphManager = new BarGraph('#doors-bar-area', MapManager, targets[0]);
            ZipcodeManager = new Zipcode("input[name='zipcode']", zipcode);
            PrecinctManager = new Precinct("input[name='precinct']", targets[0].precints, centroid);
            tailoredInfoManager = new TailoredInfo("#action-list-area", MapManager, targets[0].precints, actions[0].events, centroid, source);
          }, 500);
          //Hide loader
          setTimeout(()=>{$("#beto-loader").fadeOut(100);}, 500);

      })
    });
  });
};

// First time the map loads
$(document).on("map.style.loaded", () => {

  // Oct 10 '18 (RC) Zipcode - Prefill: Check if we can trigger the app to read queries
  if (Object.keys(queryString).length <= 2 && queryString.zipcode ) {
    ZipcodeManager.pushZip(queryString.zipcode);
  } else if (Object.keys(queryString).length <= 2 && queryString.precint_key) {
    PrecinctManager.pushPrecinct(queryString.precint_key);
  } else {
    $(window).trigger("window.applyStates", queryString);
  }

  // Check boxes
  const filters = ['show_hq', 'show_popups', 'show_events', 'ctrl-road'];
  for (let i in filters) {
    if(queryString[filters[i]]) {
      $(`input[name=${filters[i]}]`).prop("checked", queryString[filters[i]] == "true")
    }
  }

  if(queryString['show_strategy']) {
    $(`input[name=show_strategy][value=${queryString['show_strategy']}]`).prop("checked", true);
  }

  $(function () {
    // $('[data-toggle="tooltip"]').tooltip()
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
  })

});

// Listen to changes in query params
window.addEventListener('popstate', (event) => {
  const params = $.deparam(event.currentTarget ? event.currentTarget.location.search.substring(1) : null);
  $(window).trigger('window.applyStates', params);
}, false);

// General Event Listener
$(window).on('window.applyStates', (event, options) => {
  if (options.action) {
    if(options.action === "precint-click") {
      $("#action-list-area-tab").trigger("click");
    } else if(options.action === "zipcode-search" ||
              options.action === "precinct-search") {
      $("#action-list-area-tab").trigger("click");
    }

    tailoredInfoManager.setFocus(options);
  }
  MapManager.refreshMap(options);
});

// Add listener for mobile show-more-button..
$(() => {
  $(document).on('click', "a#show-more-btn", (e) => {
    $(e.currentTarget).toggleClass("open");
    $("#main-content-area").toggleClass("open");
  });
});
