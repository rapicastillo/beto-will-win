import HistoryHandler from "../history/HistoryHandler";
import helper from "../helper";
export default class TailoredInfo {
  constructor(container, mapManager, targetData, actionData, centroids, source) {
    this.d3Container = d3.select(container);
    this.sourceUrl = source;
    this.mapManager = mapManager;
    this.centroids = centroids;
    this.geojsonLayer = this.mapManager.geojsonLayer;
    this.targetData = targetData;
    this.eventsData = actionData.filter(item => !(item.is_popup || item.is_office || item.is_grassroots_office));
    this.stagingSites = actionData.filter(item => item.is_popup || item.is_office || item.is_grassroots_office);
    this.mileRadius = 20;

    // this.render({lat: 29.887645922330883, lng:-97.94897460937501});

    // Initialize listener
    $("#action-list-area").on("scroll", (e) => {
      const boxOffset = ($("#action-tabs-content").offset().top - $(".event-popup-tabs-area").height()) - 30;
      if (boxOffset < $("#action-list-area").offset().top) {
        $(".event-popup-tabs-area").addClass("x-sticky");
      } else {
        $(".event-popup-tabs-area").removeClass("x-sticky");
      }
    });
  }


  render(options = null) {

    this.d3Container.selectAll("*").remove();

    if (options.action === "precint-click" || options.action == "precinct-search") {
      const d = this.targetData.filter(item => item.key.toString() === options.precint_key.toString())[0];

      const _commaFormat = d3.format(',');
      const _rem = d.cp;
      const titleArea = this.d3Container.append("div").attr("class", "gen-title");
      titleArea.html(`
        <h5>
          <img src='/img/door.png' class='overall-image' />
          <span>${d.cnty}-${d.key.substring((d.fips+"").length)}</span>
        </h5>
        <div class='precint-line'>
          <div class='breakdown-title'>
            <span class='breakdown-label'>
              <span class='breakdown-label-title'>Universe Targets</span>
            </span>
            <span class='breakdown-count prec-counter'><strong data-count="${_rem}">${_commaFormat(_rem)}</strong><span>&nbsp;/&nbsp;${_commaFormat(d.tg)}</span></span>
          </div>
          <div class='overall-breakdown'>
            <span class='overall-target'></span>
            <span class='overall-achieved' style='width: ${(_rem * 100)/ d.tg}%'></span>
            <span class='overall-voted' style='width: ${(d.vtd * 100)/ d.tg}%'></span>
          </div>
          <div class='overall-voted-lbl'>
            <span>Early Voter Doors:</span><span class='overall-voted-val'>${_commaFormat(d.vtd)}</span>
          </div>

          ${this.renderOtherStats({kn: d.kn,
                                  cl: d.cl,
                                  vp: d.vp,
                                  vtd: d.vtd
                                })}

        </div>
      `)
    }


    const infoArea = this.d3Container
      .append("div").attr("class", "gen-basic-data");

    const nearestTopsData = this.getNearestPriorities(options.lat, options.lng, options.precint_key);
    const nearestTops = this.d3Container
                          .append("div").attr("class", "gen-nearest-tops");
    nearestTops.append('h5').html(`<i class='fa fa-location-arrow'></i><span>Nearby Priority Precincts</span>`);
    const precintData = nearestTops.append("div").attr('class', 'precint-data');
    precintData.selectAll("div.gen-nearest-tops-item")
      .data(nearestTopsData, (precint) => precint.key)
      .enter()
        .append("div")
          .attr("class", "gen-nearby-item")
          .html(this.renderPrecintData.bind(this))
          .on("dblclick", (d) => {
            var center = this.mapManager.getPrecintCoords(d.key) || {};
            HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: d.key });
            this.mapManager.setPoint(d.key);
          })
          .on("click",
              (d)=>{
                  var center = this.mapManager.getPrecintCoords(d.key) || {};
                  HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: d.key });
                  this.mapManager.setPoint(d.key);
              }
          )
          .on("mouseover", (d) => {
            this.mapManager.showPoint(d.key);
            this.mapManager.showLayerInfo(d);
          })
        .exit();

    // Set tab..

    this.d3Container
      .append("div").attr("class", "event-popup-tabs-area")
      .html(`
        <ul class="nav nav-tabs" id="action-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active show" id="popup-offices-tab" data-toggle="tab" href="#popup-offices" role="tab" aria-controls="popup-offices" aria-selected="true">Pop-up offices</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="events-tab" data-toggle="tab" href="#events-area" role="tab" aria-controls="events-area" aria-selected="false">Events</a>
          </li>
        </ul>
        `);

    const tabContent = this.d3Container.append("div").attr("class", "tab-content").attr("id", "action-tabs-content")
    // Get Nearest Staging Sites
    const nearbyStaging = this.stagingSites; //.filter(item => helper.getDistanceInMiles(options.lat, options.lng, item.lat, item.lng) < this.mileRadius);
    const popupOfficesNearby = nearbyStaging.map(item => Object.assign(item, {distance: helper.getDistanceInMiles(options.lat, options.lng, item.lat, item.lng)}))
                                .filter(item=> item.distance < 100).sort((x, y) => d3.ascending(x.distance, y.distance));
    // const stagingWithDistance = popupOfficesNearby;
    const nearestStagingSites = tabContent.append("div")
                                  .attr("class", "gen-nearest-staging tab-pane active show")
                                  .attr("id", "popup-offices")
                                  .attr("role", "tabpanel");
      nearestStagingSites.append("h5").html(`<i class='fa fa-home'></i><span>Nearest Pop-up office</span>`);
      nearestStagingSites.selectAll("div.gen-nearby-item")
        .data(popupOfficesNearby, d => d.url)
          .enter()
            .append("div")
            .attr("class", "gen-nearby-item gen-staging")
            .html(d => this.renderStagingSite(d, this.sourceUrl))
          .exit();


    // Events
    const nearbyEvents = this.eventsData;//.filter(item => helper.getDistanceInMiles(options.lat, options.lng, item.lat, item.lng) < this.mileRadius);
    const eventsWithDistance = nearbyEvents.map(item => Object.assign(item, {distance: helper.getDistanceInMiles(options.lat, options.lng, item.lat, item.lng)})).sort((x, y) => d3.ascending(x.distance, y.distance));

    const nearestBlockwalk = eventsWithDistance.find(item => item.categories === "blockwalk");

    const eventsContainer = tabContent.append("div")
                              .attr("class", "events-area-container tab-pane")
                              .attr("id", "events-area")
                              .attr("role", "tabpanel");
    // Closest blockwalk
    eventsContainer.append("div").attr("id", "nearest-blockwalk").html(`<h5><img src='/img/clipboard.svg' width="20"></i><span>Nearest Blockwalk</span></h5>${this.renderEvent(nearestBlockwalk, this.sourceUrl)}`);


    // Get Nearest Events

    const nearestEvents = eventsContainer.append("div").attr("class", "gen-nearest-events");
    nearestEvents.append("h5").html(`<img src='/img/volunteer.png' width="20"/><span>Nearest Events</span>`);
    nearestEvents.selectAll("div.gen-nearby-item")
      .data(eventsWithDistance.splice(0,100), d => d.url)
        .enter()
          .append("div")
          .attr("class", "gen-nearby-item gen-events")
          .html(d => this.renderEvent(d, this.sourceUrl))
        .exit();
  }

  getNearestPriorities(lat, lng, precint_key = null) {
    const candidates = this.centroids.filter(item => {
      if (item.PCTKEY === precint_key) {
        return false;
      };

      item.distance = helper.getDistanceInMiles(item.latitude, item.longitude, lat, lng);
      return item.distance < this.mileRadius;
    }).map(item => item.PCTKEY);

    const precints = this.targetData
                        .filter(item => candidates.includes(item.key) && item.pr !== "0")
                        .sort((x,y) => d3.ascending(x.pr, y.pr))
                        .slice(0,8);
    // const nearestPrecints = this.geojsonLayer.getLayers().filter(item => {
    //   const center = item.getCenter();
    //   item.distance = helper.getDistanceInMiles(center.lat, center.lng, lat, lng);
    //   return item.distance < this.mileRadius;
    // }).sort((x,y) => d3.descending(x.target.density, y.target.density)).slice(0,5);
    // return nearestPrecints;
    return precints;
  }
  renderStatsInfo(){
    return `
      <div class="info-popup">
        <table class="table">
          <tr>
            <th>Doors Knocked<br/>All Time</th>
            <td>All door attempts since the beginning of the campaign. If a door was knocked twice, it will count twice.</td>
          </tr>
          <tr>
            <th>Calls Made<br/>All Time</th>
            <td>All call attempts since the beginning of the campaign. If a phone was called twice, it will count twice.</td>
          </tr>
          <tr>
            <th>Vote Plans Made<br/>All Time</th>
            <td>Number of vote plans that have been made at the doors or over the phone.</td>
          </tr>
        </table>
      </div>
    `.replace(/(?:\r\n|\r|\n)/g, '');
  }

  renderOtherStats(d) {
    const _comma = d3.format(',');
    const _formatData = (num) => num > 10000 ? d3.format(".2s")(num) : d3.format(',')(num);
    return `
    <div class='breakdown-other-cont'>
      <div class='breakdown-title breakdown-title-other'>
        <span class='breakdown-label'>
          <span>Other Stats</span>
          <i class='fa fa-info-circle'></i>
        </span>
        ${this.renderStatsInfo()}
      </div>
      <div class='mode-dets more-dets-overall-breakdown'>
        <div class='more-dets more-dets-total-knocks'>
          <span class='breakdown-label'>Doors Knocked</span>
          <div class='more-dets-value' data-toggle='tooltip' title='${_comma(d.kn)} Door${d.kn == 1 ?'':'s'} Knocked'>
            <span class='more-dets-num'>${_formatData(d.kn)}</span>
            <span class='more-dets-lbl'>Doors Knocked<br/>All Time</span>
          </div>
        </div>

        <div class='more-dets more-dets-total-knocks'>
          <span class='breakdown-label'>Total Calls</span>
          <div class='more-dets-value' data-toggle='tooltip' title='${_comma(d.cl)} Call${d.cl == 1 ?'':'s'} Made'>
            <span class='more-dets-num'>${_formatData(d.cl)}</span>
            <span class='more-dets-lbl'>Calls Made<br/>All Time</span>
          </div>
        </div>

        <div class='more-dets more-dets-total-knocks'>
          <span class='breakdown-label'>Vote Plans</span>
          <div class='more-dets-value' data-toggle='tooltip' title='${_comma(d.vp)} Vote Plan${d.vp == 1 ?'':'s'} Made'>
            <span class='more-dets-num'>${_formatData(d.vp)}</span>
            <span class='more-dets-lbl'>Vote Plans Made<br/>All Time</span>
          </div>
        </div>
      </div>
    </div>
    `;
  }

  renderEvent(event, source) {
    return `
      <div class='event-detail-container'>
        <div class='event-subhead'>
          <h5 class='event-distance'>~${event.distance.toFixed(2)}mi</h5>
          <h5 class='event-categ'>${event.categories}</h5>
        </div>
        <h4><a target='_blank' href='${event.url}?source=${source}'>${event.title}</a></h4>
        <div class='event-details'>
          <div class='event-dateplace'>
            <p class='event-date'>${moment(`${event.start_datetime}`).format("MMM D, YYYY • HH:mm a")}</p>
            <p class='event-venue'>${event.venue}</p>
          </div>
          <div class='event-rsvp'>
            <a href='${event.url}?source=${source}' target='_blank' class='beto-cta'>RSVP</a>
          </div>
        </div>
      </div>
    `;
  }

  //<h5 class='staging-categ'>${staging.is_office ? "Official HQ" : staging.is_popup ? 'Pop-up Office' : 'Grassroots HQ'}</h5>
  renderStagingSite(staging, source) {
    return `
      <div class='staging-detail-container'>
        <div class='staging-subhead'>
          <h5 class='staging-distance'>~${staging.distance.toFixed(2)}mi</h5>

        </div>
        <h4><a target='_blank' href='${staging.url}?source=${source}'>${staging.title}</a></h4>
        <div class='staging-details'>
          <div class='staging-dateplace'>
            <p class='staging-description'>${staging.venue}</p>
            <div class='staging-description'>
              ${!!staging.event_popuphours ? staging.event_popuphours.split(';').map(i=>`<div>${i}</div>`).join("") : ""}
              <div>${!!staging.event_popupcontacts ? staging.event_popupcontacts : ""}</div>
            </div>
          </div>
          <div class='staging-rsvp'>
            <a href='${staging.url}?source=${source}' target='_blank' class='beto-cta'>VOLUNTEER</a>
          </div>
        </div>
      </div>
    `;
  }

  getScore(urgScore) {
    switch (urgScore) {
      case 5: return 'l5';
      case 4: return 'l4';
      case 3: return 'l3';
      case 2: return 'l2';
      case 1: return 'l1';
    }
    return '0';
  }

  renderPrecintData(d) {
    const _commaFormat = d3.format(',');
    const percComplete = (d.cp/d.tg);
    const _rem = d.cp;
    return `<div class='precint-line' style='--perc: ${(d.vtd * 100)/ d.tg}%' data-voted='${_commaFormat(d.vtd)}'>
              <div class='breakdown-title'>
                <span class='prec-name breakdown-label' data-urgscore="${this.getScore(d.pr)}">
                  ${d.cnty}-${d.key.substring((d.fips+"").length)}
                </span>
                <span class='breakdown-count prec-counter'><strong data-count="${_rem}">${_commaFormat(_rem)}</strong><span>&nbsp;/&nbsp;${_commaFormat(d.tg)}</span></span>
              </div>
              <div class='overall-breakdown'>
                <span class='overall-target'></span>
                <span class='overall-achieved' style='width: ${(_rem * 100)/ d.tg}%'></span>
                <span class='overall-voted' style='width: ${(d.vtd * 100)/ d.tg}%;'></span>
              </div>
            </div>`;
  }


  setFocus(o) {

    switch(o.action) {
      case "precinct-search":
      case "zipcode-search":
      case "precint-click":
        if(o.lat && o.lng) {
          this.render(o);
        }
      break;
    }
  }
}
