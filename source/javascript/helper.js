
const _format = d3.format(".1s");
const _commaFormat = d3.format(",");

export const toMile = (meter) =>meter * 0.00062137;

export const getDistance = (lat1, lng1, lat2, lng2) => {
  const latLng1 = L.latLng([lat1, lng1]);
  const latLng2 = L.latLng([lat2, lng2]);
  return latLng1.distanceTo(latLng2);
};

export const getDistanceInMiles = (lat1, lng1, lat2, lng2) => {
  return toMile(getDistance(lat1, lng1, lat2, lng2));
};

export const counterText = (remaining, target) => {
  const _rem = target - remaining;
  return `${_rem >= 10000 ? _format(_rem) : _commaFormat(_rem)} / ${target >= 10000 ? _format(target) : _commaFormat(target)}`;
};

export const mobileEvent = (mobileEventListener = (e)=>{}, nonMobileEventListener = (e)=>{}) => {
  if (window.innerWidth <= 850) {
    return mobileEventListener;
  } else {
    return nonMobileEventListener;
  }
};

export const getQueryString = () => {
    var queryStringKeyValue = window.parent.location.search.replace('?', '').split('&');
    var qsJsonObject = {};
    if (queryStringKeyValue != '') {
        for (let i = 0; i < queryStringKeyValue.length; i++) {
            qsJsonObject[queryStringKeyValue[i].split('=')[0]] = queryStringKeyValue[i].split('=')[1];
        }
    }
    return qsJsonObject;
};

export default {
  toMile, getDistance, getDistanceInMiles, counterText, mobileEvent, getQueryString
};
