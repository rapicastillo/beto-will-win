import HistoryHandler from "../history/HistoryHandler";

export default class Zipcode {

  constructor(target, data = null) {
    this.zipcodeData = data.reduce(function (zips, item) {
          zips[item.zip] = item;return zips;
    },{});

    this.zipcodeInput = $(target);
    this.initializeEvents();

    this.currentZipcode = null;
  }

  initializeEvents() {
    let _this = this;
    this.zipcodeInput.closest("form").on("submit", (e)=>{
      e.preventDefault();
      return false;
    })
    this.zipcodeInput.on("keyup", (e) => {
      if (e.target.value.length == 5) {
        if (this.currentZipcode != e.target.value) {
          this.currentZipcode = e.target.value;
          _this.pushZip(e.target.value);
        }
      }
    });
  }

  pushZip(zipcode) {
    const target = this.zipcodeData[zipcode];
    HistoryHandler.push({action: 'zipcode-search', zipcode: zipcode, lat: target.lat, lng: target.lon });

    if (this.zipcodeInput.val() != zipcode) {
      this.zipcodeInput.val(zipcode);
    }
  }
}
