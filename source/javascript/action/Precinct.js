import HistoryHandler from "../history/HistoryHandler";

export default class Precinct {

  constructor(target, precincts, data = null) {
    this.precincts = precincts;
    this.precinctData = data.reduce(function (precincts, item) {
          precincts[item.PCTKEY] = item;
          return precincts;
    },{});

    this.precinctInput = $(target).typeahead({
        hint: true,
        highlight: true,
        minLength: 2
      },
      {
        name: 'precincts',
        source: this.matcher(this.precincts),
        display: this.renderPrecinctName
      }
    )

    this.initializeEvents();

    this.currentPrecinct = null;
  }

  renderPrecinctName(target) {
    return `${target.cnty}-${target.key.substring(target.fips.toString().length)}`;
  }

  matcher(precincts) {
    return function findMatches(q, cb) {
      let matches = precincts
                      .filter(i => `${i.cnty.toLowerCase()}-${i.key.substring(i.fips.toString().length)}`.indexOf(q.toLowerCase()) == 0);

      cb(matches);
    }
  }

  initializeEvents() {
    let _this = this;
    this.precinctInput.closest("form").on("submit", (e)=>{
      e.preventDefault();
      return false;
    })

    this.precinctInput.on('typeahead:selected', function (obj, datum) {
        if(datum) {
          _this.pushPrecinct(datum.key)
        }
    })
    .on("keyup",(e) => {

        if (e.keyCode == 13) {
          this.matcher(this.precincts)(e.currentTarget.value, m => this.pushPrecinct(m[0].key));
        }
      });
  }

  retrievePrecinct(key) {
    return this.precincts.filter(i => i.key == key)[0];
  }

  pushPrecinct(precinct) {
    const target = this.precinctData[precinct];
    if (target !== undefined) {
      HistoryHandler.push({action: 'precinct-search', precint_key: precinct, lat: target.latitude, lng: target.longitude });
      const prec = this.retrievePrecinct(target.PCTKEY);

      if (this.precinctInput.val() != this.renderPrecinctName(prec)) {
        this.precinctInput.val(this.renderPrecinctName(prec));
      }
    }

  }
}
