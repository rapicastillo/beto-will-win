import HistoryHandler from "../history/HistoryHandler";
import helper, { mobileEvent } from "../helper";

export default class BarGraph {
  constructor (container, mapManager, targets) {

    this.mapManager= mapManager;
    this.d3Container = d3.select(container);
    this.targets = targets;

    this.nearby = [];

    this.dom = document.querySelector(container);
    this.dom.innerHTML = `<div class='bar-overall'>
                          </div>
                          <div class='bar-top-priority data-point'>
                              <h5>
                                <i class="fa fa-location-arrow"></i>
                                <span>Top Priority</span>
                              </h5>
                              <div class='bar-priority-data'></div>
                          </div>
                          <div class='bar-counties data-point'>
                              <h5>
                                <i class="fa fa-pie-chart"></i>
                                <span>By County</span>
                              </h5>
                              <div class='bar-counties-data'></div>
                          </div>`;

    // this.countyScaling =  d3.scaleLinear()
    //                 .domain([0, d3.max(this.targets.counties, (d) => d.target)])
    //                 .range([0, 100]);

    this.render();
  }

  render() {
    this.renderOverall();
    this.renderPriorityData();
    this.renderCountyData();
  }

  renderNearbyData() {
    // Get zipcode location
    // Get nearby counties by geoJson
    // list!
  }
  renderStatsInfo(){
    return `
      <div class="info-popup">
        <table class="table">
          <tr>
            <th>Doors Knocked<br/>All Time</th>
            <td>All door attempts since the beginning of the campaign. If a door was knocked twice, it will count twice.</td>
          </tr>
          <tr>
            <th>Calls Made<br/>All Time</th>
            <td>All call attempts since the beginning of the campaign. If a phone was called twice, it will count twice.</td>
          </tr>
          <tr>
            <th>Vote Plans Made<br/>All Time</th>
            <td>Number of vote plans that have been made at the doors or over the phone.</td>
          </tr>
        </table>
      </div>
    `.replace(/(?:\r\n|\r|\n)/g, '');
  }

  renderOtherStats(d) {
    const _comma = d3.format(',');
    const _formatData = (num) => num > 10000 ? d3.format(".2s")(num) : d3.format(',')(num);

    return `
    <div class='breakdown-other-cont'>
      <div class='breakdown-title breakdown-title-other'>
        <span class='breakdown-label'>
          <span>Other Stats</span>
          <i class='fa fa-info-circle'></i>
        </span>
        ${this.renderStatsInfo()}
      </div>
      <div class='mode-dets more-dets-overall-breakdown'>
        <div class='more-dets more-dets-total-knocks'>
          <span class='breakdown-label'>Doors Knocked</span>
          <div class='more-dets-value' data-toggle='tooltip' title='${_comma(d.kn)} Door${d.kn == 1 ?'':'s'} Knocked'>
            <span class='more-dets-num'>${_formatData(d.kn)}</span>
            <span class='more-dets-lbl'>Doors Knocked<br/>All Time</span>
          </div>
        </div>

        <div class='more-dets more-dets-total-knocks'>
          <span class='breakdown-label'>Total Calls</span>
          <div class='more-dets-value' data-toggle='tooltip' title='${_comma(d.cl)} Call${d.cl == 1 ?'':'s'} Made'>
            <span class='more-dets-num'>${_formatData(d.cl)}</span>
            <span class='more-dets-lbl'>Calls Made<br/>All Time</span>
          </div>
        </div>

        <div class='more-dets more-dets-total-knocks'>
          <span class='breakdown-label'>Vote Plans</span>
          <div class='more-dets-value' data-toggle='tooltip' title='${_comma(d.vp)} Vote Plan${d.vp == 1 ?'':'s'} Made'>
            <span class='more-dets-num'>${_formatData(d.vp)}</span>
            <span class='more-dets-lbl'>Vote Plans Made<br/>All Time</span>
          </div>
        </div>

      </div>
    </div>
    `;
  }

  renderOverall() {
    const overall = this.d3Container.select(".bar-overall");
    const overallTarget = this.targets.totals.tg;
    const overallKnocked = this.targets.totals.cp; //d3.sum(this.targets.counties, (d) => d.cp);
    const overallVoted = this.targets.totals.vtd;
    const _comma = d3.format(',');
    const _short = (num) => num > 10000 ? d3.format('.2s')(num): _comma(num);
    overall.html(`<div class='overall-area data-point'>
                    <h5>
                      <img src='/img/door.png' class='overall-image'>
                      <span>Overall GOTV</span>
                    </h5>

                    <div class='breakdown-title'>
                      <span class='breakdown-label'>
                        <span class='breakdown-label-title'>Universe Targets</span>
                      </span>
                      <span class='breakdown-count'><strong data-count="${(overallKnocked + overallVoted)}">${_comma(overallKnocked)}</strong><span>&nbsp;/&nbsp;${_comma(overallTarget)}</span></span>
                    </div>
                    <div class='overall-breakdown'>
                      <span class='overall-target'></span>
                      <span class='overall-achieved' style='width: ${(overallKnocked * 100)/ overallTarget}%'></span>
                      <span class='overall-voted' style='width: ${(overallVoted * 100)/ overallTarget}%'></span>
                    </div>
                    <div class='overall-voted-lbl'>
                      <span>Early Voter Doors:</span><span class='overall-voted-val'>${_comma(overallVoted)}</span>
                    </div>

                    ${this.renderOtherStats({
                                              kn: this.targets.totals.kn,
                                              cl: this.targets.totals.cl,
                                              vp: this.targets.totals.vp,
                                              vtd: this.targets.totals.vtd
                                            })}
                  </div>`);
  }
  getScore(urgScore) {
    switch (urgScore) {
      case 5: return 'l5';
      case 4: return 'l4';
      case 3: return 'l3';
      case 2: return 'l2';
      case 1: return 'l1';
    }
    return '0';
  }

  renderPriorityData() {
    const parent = this.d3Container.select("div.bar-priority-data");

    // const scale = this.countyScaling;
    const _format = d3.format(".1s");
    const _commaFormat = d3.format(",");

    const topPriorityData = this.targets.precints.filter(item => item.pr == 1);

    const precintDataContainer = parent
        .selectAll("div.precint-data").data(topPriorityData.slice(0, 10), (d, i) => { return d.key; })
          .enter()
          .append("div")
            .attr("class", "precint-data")
            .html((d) => {
              const percComplete = (d.cp/d.tg);
              const _rem = d.cp;
              return `<div class='precint-line' style='--perc: ${(d.vtd * 100)/ d.tg}%' data-voted='${_commaFormat(d.vtd)}'>
                        <div class='breakdown-title'>
                          <span class='prec-name breakdown-label' data-urgscore="${this.getScore(d.pr)}">
                            ${d.cnty}-${d.key.substring((d.fips+"").length)}
                          </span>
                          <span class='breakdown-count prec-counter'>
                            <strong data-count="${_rem}">${_commaFormat(_rem)}</strong><span>&nbsp;/&nbsp;${_commaFormat(d.tg)}</span>
                          </span>
                        </div>
                        <div class='overall-breakdown' >
                          <span class='overall-target'></span>
                          <span class='overall-achieved' style='width: ${(_rem * 100)/ d.tg}%'></span>
                          <span class='overall-voted' style='width: ${(d.vtd * 100)/ d.tg}%;'></span>
                        </div>
                      </div>`;
            })
            .on("click",
                (d)=>{
                  var center = this.mapManager.getPrecintCoords(d.key) || {};
                  HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: d.key });
                  this.mapManager.setPoint(d.key)
                }
            )
            .on("dblclick", (d) => {
              var center = this.mapManager.getPrecintCoords(d.key) || {};
              this.mapManager.setPoint(d.key);
              HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: d.key });
            })
            .on("mouseover", (d) => {
              this.mapManager.showLayerInfo(d);
              this.mapManager.showPoint(d.key);
            })
            .on("mouseout", (d) => {
              this.mapManager.hideInfo();
            })
            .sort((x, y) => {
               return d3.ascending(x.pr, y.pr);
            })
          .exit();

    parent.append('a')
      .attr('class', 'more-tops')
      .attr('href', 'javascript: void(null)')
      .html(`Show all top priorities <i class='fa fa-chevron-circle-right'></i>`)
      .on('click', () => {
        this.showAllPriority(topPriorityData);
      });

  }

  showAllPriority(data) {
    let countyDetail = d3.select("#county-inner-modal").attr("data-mode", "priority");
    countyDetail.select(".county-specific").html(`
      <h5>
        <img src="/img/door.png" class="overall-image">
        <span>Top Priority Counties</span>
      </h5>
    `);

    // countyDetail.select(".county-precints .bar-priority-data *").remove()
    const _commaFormat = d3.format(',');
    countyDetail.selectAll(".county-precints .bar-priority-data *").remove();
    let countyItem = countyDetail.select(".county-precints .bar-priority-data")
      .selectAll(".precint-data")
      .data(data)
              .enter()
                .append("div")
                  .attr("class", "precint-data")
                  .html((d) => {
                    const percComplete = (d.cp/d.tg);
                    const _rem = d.cp;
                    return `<div class='precint-line' style='--perc: ${(d.vtd * 100)/ d.tg}%' data-voted='${_commaFormat(d.vtd)}'>
                              <div class='breakdown-title'>
                                <span class='prec-name breakdown-label' data-urgscore="${this.getScore(d.pr)}">
                                  ${d.cnty}-${d.key.substring((d.fips+"").length)}
                                </span>
                                <span class='breakdown-count prec-counter'><strong data-count="${_rem}">${_commaFormat(_rem)}</strong><span>&nbsp;/&nbsp;${_commaFormat(d.tg)}</span></span>
                              </div>
                              <div class='overall-breakdown'>
                                <span class='overall-target'></span>
                                <span class='overall-achieved' style='width: ${(_rem * 100)/ d.tg}%'></span>
                                <span class='overall-voted' style='width: ${(d.vtd * 100)/ d.tg}%;'></span>
                              </div>
                            </div>`
                  });
                  countyItem.on("click",
                      (d)=>{
                        var center = this.mapManager.getPrecintCoords(d.key) || {};
                        this.mapManager.setPoint(d.key);
                        HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: d.key });
                      }
                  )
                  .on("dblclick", (d) => {
                    var center = this.mapManager.getPrecintCoords(d.key) || {};
                    HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: d.key });
                  })
                  .on("mouseover", (d) => {
                    this.mapManager.showPoint(d.key);
                    this.mapManager.showLayerInfo(d);
                  });
                  countyItem.exit().remove()
                  ;

    $("#county-inner-modal").animate({left: '0'}, 200);
  }

  showDetails(data) {
    let countyDetail = d3.select("#county-inner-modal");
    const total = data.cp;
    const _comma = d3.format(',');
    const _short = (num) => num > 10000 ? d3.format('.2s')(num) : _comma(num);
    countyDetail.select(".county-specific").html(`
      <h5>
        <img src="/img/door.png" class="overall-image">
        <span>${data.name} County</span>
      </h5>

      <div class="breakdown-title">
        <span class="breakdown-label">
          <span class='breakdown-label-title'>Universe Targets</span>
        </span>
        <span class="breakdown-count"><strong data-count="${total}">${_comma(total)}</strong><span>&nbsp;/&nbsp;${_comma(data.tg)}</span></span>
      </div>
      <div class="overall-breakdown">
        <span class="overall-target"></span>
        <span class="overall-achieved" style="width: ${total * 100 / data.tg}%"></span>
        <span class='overall-voted' style='width: ${(data.vtd * 100)/ data.tg}%;'></span>
      </div>
      <div class='overall-voted-lbl'>
        <span>Early Voter Doors:</span><span class='overall-voted-val'>${_comma(data.vtd)}</span>
      </div>

      ${this.renderOtherStats({kn: data.kn,
                              cl: data.cl,
                              vp: data.vp,
                              vtd: data.vtd
                            })}
    `);

    // countyDetail.select(".county-precints .bar-priority-data *").remove()
    const precintData = this.targets.precints.filter((item)=> item.fips == data.fips && item.tg > 0)
      .sort((x, y) => {
       return d3.ascending(x.pr, y.pr);
    });
    const noPriority = precintData.filter(d => d.pr == "0")
    const priorityData = precintData.filter(d=>d.pr !== "0").concat(noPriority);

    const _commaFormat = d3.format(',');
    countyDetail.selectAll(".county-precints .bar-priority-data *").remove();
    let countyItem = countyDetail.select(".county-precints .bar-priority-data")
      .selectAll(".precint-data")
      .data(priorityData)
              .enter()
                .append("div")
                  .attr("class", "precint-data")
                  .html((d) => {
                    const percComplete = (d.cp/d.tg);
                    const _rem = d.cp;
                    return `<div class='precint-line' style='--perc: ${(d.vtd * 100)/ d.tg}%' data-voted='${_commaFormat(d.vtd)}'>
                              <div class='breakdown-title'>
                                <span class='prec-name breakdown-label' data-urgscore="${this.getScore(d.pr)}">
                                  ${d.cnty}-${d.key.substring((d.fips+"").length)}
                                </span>
                                <span class='breakdown-count prec-counter'><strong data-count="${_rem}">${_commaFormat(_rem)}</strong><span>&nbsp;/&nbsp;${_commaFormat(d.tg)}</span></span>
                              </div>
                              <div class='overall-breakdown'>
                                <span class='overall-target'></span>
                                <span class='overall-achieved' style='width: ${(_rem * 100)/ d.tg}%'></span>
                                <span class='overall-voted' style='width: ${(d.vtd * 100)/ d.tg}%;'></span>
                              </div>
                            </div>`
                  });
                  countyItem.on("click",
                      (d)=>{
                        var center = this.mapManager.getPrecintCoords(d.key) || {};
                        this.mapManager.setPoint(d.key);
                        HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: d.key });
                      }
                  )
                  .on("dblclick", (d) => {
                    var center = this.mapManager.getPrecintCoords(d.key) || {};
                    HistoryHandler.push({ lat: center.lat, lng: center.lng, action: 'precint-click', precint_key: d.key });
                  })
                  .on("mouseover", (d) => {
                    this.mapManager.showPoint(d.key);
                    this.mapManager.showLayerInfo(d);
                  });
                  countyItem.exit().remove()
                  ;

    $("#county-inner-modal").animate({left: '0'}, 200);
  }

  renderCountyData() {

    const parent = this.d3Container.select("div.bar-counties-data");
    // const scale = this.countyScaling;
    const _format = d3.format(',');
    const countyItems = parent
      .selectAll("div.county-item")
      .data(this.targets.counties, (data) => data.id)
        .enter();

    const dataPoint = countyItems.append("div")
          .attr("class", "county-item data-point")
          .attr("data-fips", (data) => data.fips)
          .html((datum)=>{
                const total = datum.cp;
                return `
                <div class='bar-area' style='--perc: ${(datum.vtd * 100)/ datum.tg}%' data-voted='${_format(datum.vtd)}'>
                  <div class='breakdown-title'>
                    <span class='breakdown-label'>${datum.name} <i class='fa fa-chevron-circle-right'></i></span>
                    <span class='breakdown-count'><strong data-count="${total}">${d3.format(',')(total)}</strong><span>&nbsp;/&nbsp;${d3.format(',')(datum.tg)}</span></span>
                  </div>
                  <div class='overall-breakdown'>
                    <span class='overall-target'"></span>
                    <span class='overall-achieved' style='width: ${ (total * 100) / datum.tg }%'></span>
                    <span class='overall-voted' style='width: ${(datum.vtd * 100)/ datum.tg}%;'></span>
                  </div>
                </div>
                `;
              })
              .on("click", data => {
                this.showDetails(data);
              })
              .sort((x, y) => {
                 return d3.descending(x.tg, y.tg);
              });


    countyItems.exit();

    $(document).on("click", ".bar-area", (e) =>{

      // $(".expanded-precint-data").removeClass("expanded-precint-data");
      // $(e.currentTarget).siblings("div").addClass("expanded-precint-data");
      // $(e.target).siblings("div").addClass("expanded-precint-data");
    });

    $(document).on("click", "#county-inner-modal .back-button", (e)=> {
      $("#county-inner-modal").animate({left: "-100%"}, 200);
    })
  }
}
