import HistoryHandler from "../history/HistoryHandler";
import { getDistanceInMiles } from "../helper";

export default class StagingSites {

  constructor(container, data, categories) {
    this.container = d3.select(container);
    this.stagingData = data.filter(item => ["officialhq", "grassrootshq"].includes(item.categories));
    this.categories = categories;
    this.render();
  }

  render(o = null) {

    if (o && o.lat && o.lng) {
      this.stagingData = this.stagingData.map(i => {
        i.distance= getDistanceInMiles(o.lat, o.lng, i.lat, i.lng);
        return i;
      });
    }

    this.container.selectAll("div.stagingItem").remove();

    const stagingItems = this.container
      .selectAll("div.stagingItem")
      .data(this.stagingData, d => d.url)
      .enter()
      .append("div")
        .attr("class", "stagingItem")
        .html(this.renderStagingItem.bind(this));

    if (o && o.lat && o.lng) {
      stagingItems.sort((x,y)=>d3.ascending(x.distance, y.distance));
    }

    stagingItems.exit();

  }


  refresh() {

  }

  renderStagingItem(staging) {
    return `
      <div class='staging-detail-container'>

        <div class='event-subhead'>
          ${staging.distance ? `<h5 class='staging-distance'>~${staging.distance.toFixed(2)}mi</h5>` : ""}
          <h5 class='staging-categ'>${this.categories[staging.categories].label}</h5>
        </div>

        <h4><a target='_blank' href='${staging.url}'>${staging.title}</a></h4>
        <div class='staging-details'>
          <div class='staging-dateplace'>
            <p class='staging-description'>${staging.public_description}</p>
            <p class='staging-venue'>${staging.venue}</p>
          </div>
          <div class='staging-rsvp'>
            <a href='${staging.url}' target='_blank' class='beto-cta'>VOLUNTEER</a>
          </div>
        </div>
      </div>
    `;
  }
}
