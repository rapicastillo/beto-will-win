import HistoryHandler from "../history/HistoryHandler";
import { getDistanceInMiles } from "../helper";
export default class Events {

  constructor(container, data, categories) {
    this.container = d3.select(container);
    this.eventsData = data;
    this.categories = categories;
    this.categories.officialhq.hidden = true;
    this.categories.grassrootshq.hidden = true;
    this.categories.popupoffice.hidden = true;
    this.render();
  }

  render() {

    const eventListArea = this.container.append("div");

    eventListArea.attr("id", "event-listarea")
      .html("<h4>Events</h4>");

    d3.select("div#event-listarea").selectAll("div.eventItem")
      .data(this.eventsData.filter(item => {
          const categs = item.categories.split(',');
          if (categs.length == 1) {
            return !this.categories[item.categories].hidden;
          } else {
            for (let i in categs) {
              if (!this.categories[categs[i]].hidden)  {
                return true;
              }
            }
            return false;
          }
        }
      ), d => d.url)
      .enter()
      .append("div")
        .attr("class", "eventItem")
        .html(this.renderEventItem.bind(this))
      .exit();
  }

  refresh(o) {

    if(o.lat && o.lng) {
      this.eventsData = this.eventsData
                          .filter(item => {
                              const categs = item.categories.split(',');
                              if (categs.length == 1) {
                                return !this.categories[item.categories].hidden;
                              } else {
                                for (let i in categs) {
                                  if (!this.categories[categs[i]].hidden)  {
                                    return true;
                                  }
                                }
                                return false;
                              }
                            })
                          .map(i=>{ i.distance = getDistanceInMiles(i.lat, i.lng, o.lat, o.lng);
                                    return i;
                          })
                          .sort((x, y) => d3.ascending(x.distance, y.distance));

      const firstBlockWalk = this.eventsData.find(item => item.categories == "blockwalk");

      this.container.select("div#event-listarea").remove();
      this.container.select("div#closest-blockwalk").remove();

      this.container.append("div").attr("id", "closest-blockwalk").html(
        `<h4>Closest Block Walk</h4>
          ${this.renderEventItem(firstBlockWalk)}
        `
      );

      const eventListArea = this.container
        .append("div")
        .attr("id", "event-listarea")
        .html("<h4>More events</h4>");

        eventListArea.selectAll("div.eventItem")
        .data(this.eventsData.filter(item => !this.categories[item.categories].hidden), d => d.url)
        .enter()
        .append("div")
          .attr("class", "eventItem")
          .html(this.renderEventItem.bind(this))
        .exit();
    }
  }

  renderEventItem(event) {
    return `
      <div class='event-detail-container'>
        <div class='event-subhead'>
          ${event.distance ? `<h5 class='event-distance'>~${event.distance.toFixed(2)}mi</h5>` : ""}
          <h5 class='event-categ'>${this.categories[event.categories].label}</h5>
        </div>
        <h4><a target='_blank' href='${event.url}'>${event.title}</a></h4>
        <div class='event-details'>
          <div class='event-dateplace'>
            <p class='event-date'>${moment(`${event.start_datetime}`).format("MMM D, YYYY • HH:mm a")}</p>
            <p class='event-venue'>${event.venue}</p>
          </div>
          <div class='event-rsvp'>
            <a href='${event.url}' target='_blank' class='beto-cta'>RSVP</a>
          </div>
        </div>
      </div>
    `;
  }
}
