"use strict";

var _map = require("./map");

var _map2 = _interopRequireDefault(_map);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

console.log("LOVE");
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function () {

  var map = L.map('map').setView([31.9686, -99.9018], 7);
  L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
    attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
    maxZoom: 16 }).addTo(map);

  var doorKnockLayer = L.featureGroup().addTo(map);
  var shiftLayer = L.featureGroup().addTo(map);
  var stagingLayer = L.markerClusterGroup({
    spiderfyDistanceMultiplier: 5,
    iconCreateFunction: function iconCreateFunction(cluster) {
      var remaining = cluster.getAllChildMarkers().map(function (item) {
        return item.options.targets.remaining;
      }).reduce(function (total, num) {
        return total + num;
      });
      var marker = void 0;
      if (remaining < 50) {
        marker = "marker-cluster-small";
      } else if (remaining >= 50 && remaining < 100) {
        marker = "marker-cluster-medium";
      } else {
        marker = "marker-cluster-large";
      }

      return L.divIcon({
        html: '<div><span>' + remaining + '</span></div>',
        className: "marker-cluster " + marker
      });
    }
  }).addTo(map);
  var geojsonLayer;

  // leaflet-marker-icon marker-cluster marker-cluster-small leaflet-zoom-animated leaflet-interactive
  // leaflet-marker-icon marker-cluster marker-cluster-small leaflet-zoom-animated leaflet-interactive

  $.getJSON("./data/tx_counties.geojson").then(function (data) {
    geojsonLayer = L.geoJson(data, {
      onEachFeature: function onEachFeature(feature, layer) {
        layer._leaflet_id = feature.properties.FIPS;
      } });
    geojsonLayer.addTo(doorKnockLayer);
    return;
  }).then(function () {
    return $.getJSON("./data/derived/targets.json");
  }).then(function (data) {
    data.counties.forEach(function (item) {
      var layer = geojsonLayer.getLayer(item.fips); //your feature id here
      var calc = item.remaining / item.target;
      layer.feature['targets'] = item;
      layer.setStyle({
        fillOpacity: 1,
        fillColor: "rgba(255,0,0," + item.urgency + ")",
        color: "#efefef",
        weight: 2,
        opacity: 0.4
      });

      var percComplete = 1 - Math.round(parseFloat(item.remaining) / parseFloat(item.target) * 100) / 100;
      percComplete = Math.round(percComplete * 100) / 100;
      var popupHTML = '<div><h3>' + layer.feature.properties.COUNTY + '</h3>\n                                <div>\n                                  <table>\n                                    <tr><th>Targets</th><th></th></tr>\n                                    <tr><td>Target Doors</td><td>' + item.target + '</td></tr>\n                                    <tr><td>Remaining Doors</td><td>' + item.remaining + '</td></tr>\n                                    <tr><td>Percent Complete</td><td>' + percComplete + '%</td></tr>\n                                    <tr><td>Urgency Score</td><td>' + item.urgency + '</td></tr>\n                                  </table>\n                                </div>\n                              </div>';
      layer.bindPopup(popupHTML).on('mouseover', function (e) {
        var popup = e.target.getPopup();
        popup.setLatLng(e.latlng).openOn(map);
        e.target.setStyle({ color: "blue" });
      }).on('mouseout', function (e) {
        this.closePopup();
        e.target.setStyle({ color: "#efefef" });
      }).on('mousemove', function (e) {
        var popup = e.target.getPopup();
        popup.setLatLng(e.latlng).openOn(map);
      });
    });
    return;
  })
  //Clustering of staging
  .then(function () {
    return $.getJSON("./data/sample-staging.json");
  }).then(function (stagingSites) {
    stagingSites.forEach(function (item) {
      var _this = this;

      if (item.remaining == item.target) {
        return;
      }

      var layer = geojsonLayer.getLayer(item.fips);
      var m = L.marker(layer.getCenter(), {
        targets: item,
        icon: new L.DivIcon({
          className: 'my-div-icon',
          html: '<div class="icon-container"><img class="my-div-image" src="/img/marker"/>' + ('<span class="my-div-span">' + item.remaining + '/' + item.target + '</span></div>')
        })
      });

      m.bindPopup('<div>' + item.remaining + ' Volunteer HQ\'s needed to set <br/>up in ' + layer.feature.targets.name + ' County</div><br/><div><strong>Click to start a staging site</strong></div>', { offset: [-4, -20] });

      m.on('mouseover', function (e) {
        this.openPopup();
      }).on('mouseout', function (e) {
        _this.closePopup();
      }).on('click', function (e) {
        window.open('/start.html');
      });

      m._leaflet_id = "marker" + item.fips;
      // m.addTo(stagingLayer);
      stagingLayer.addLayer(m);
    });
    return;
  });

  // Tab events
};
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsIm1hcC9pbmRleC5qcyJdLCJuYW1lcyI6WyJjb25zb2xlIiwibG9nIiwibWFwIiwiTCIsInNldFZpZXciLCJ0aWxlTGF5ZXIiLCJhdHRyaWJ1dGlvbiIsIm1heFpvb20iLCJhZGRUbyIsImRvb3JLbm9ja0xheWVyIiwiZmVhdHVyZUdyb3VwIiwic2hpZnRMYXllciIsInN0YWdpbmdMYXllciIsIm1hcmtlckNsdXN0ZXJHcm91cCIsInNwaWRlcmZ5RGlzdGFuY2VNdWx0aXBsaWVyIiwiaWNvbkNyZWF0ZUZ1bmN0aW9uIiwiY2x1c3RlciIsInJlbWFpbmluZyIsImdldEFsbENoaWxkTWFya2VycyIsIml0ZW0iLCJvcHRpb25zIiwidGFyZ2V0cyIsInJlZHVjZSIsInRvdGFsIiwibnVtIiwibWFya2VyIiwiZGl2SWNvbiIsImh0bWwiLCJjbGFzc05hbWUiLCJnZW9qc29uTGF5ZXIiLCIkIiwiZ2V0SlNPTiIsInRoZW4iLCJkYXRhIiwiZ2VvSnNvbiIsIm9uRWFjaEZlYXR1cmUiLCJmZWF0dXJlIiwibGF5ZXIiLCJfbGVhZmxldF9pZCIsInByb3BlcnRpZXMiLCJGSVBTIiwiY291bnRpZXMiLCJmb3JFYWNoIiwiZ2V0TGF5ZXIiLCJmaXBzIiwiY2FsYyIsInRhcmdldCIsInNldFN0eWxlIiwiZmlsbE9wYWNpdHkiLCJmaWxsQ29sb3IiLCJ1cmdlbmN5IiwiY29sb3IiLCJ3ZWlnaHQiLCJvcGFjaXR5IiwicGVyY0NvbXBsZXRlIiwiTWF0aCIsInJvdW5kIiwicGFyc2VGbG9hdCIsInBvcHVwSFRNTCIsIkNPVU5UWSIsImJpbmRQb3B1cCIsIm9uIiwiZSIsInBvcHVwIiwiZ2V0UG9wdXAiLCJzZXRMYXRMbmciLCJsYXRsbmciLCJvcGVuT24iLCJjbG9zZVBvcHVwIiwic3RhZ2luZ1NpdGVzIiwibSIsImdldENlbnRlciIsImljb24iLCJEaXZJY29uIiwibmFtZSIsIm9mZnNldCIsIm9wZW5Qb3B1cCIsIndpbmRvdyIsIm9wZW4iLCJhZGRMYXllciJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7O0FBRUFBLFFBQVFDLEdBQVIsQ0FBWSxNQUFaOzs7Ozs7O2tCQ0ZlLFlBQVc7O0FBRXRCLE1BQUlDLE1BQU1DLEVBQUVELEdBQUYsQ0FBTSxLQUFOLEVBQWFFLE9BQWIsQ0FBcUIsQ0FBQyxPQUFELEVBQVUsQ0FBQyxPQUFYLENBQXJCLEVBQTBDLENBQTFDLENBQVY7QUFDQUQsSUFBRUUsU0FBRixDQUFZLDhHQUFaLEVBQTRIO0FBQ3hIQyxpQkFBYSxpREFEMkc7QUFFeEhDLGFBQVMsRUFGK0csRUFBNUgsRUFFa0JDLEtBRmxCLENBRXdCTixHQUZ4Qjs7QUFLQSxNQUFJTyxpQkFBaUJOLEVBQUVPLFlBQUYsR0FBaUJGLEtBQWpCLENBQXVCTixHQUF2QixDQUFyQjtBQUNBLE1BQUlTLGFBQWFSLEVBQUVPLFlBQUYsR0FBaUJGLEtBQWpCLENBQXVCTixHQUF2QixDQUFqQjtBQUNBLE1BQUlVLGVBQWVULEVBQUVVLGtCQUFGLENBQXFCO0FBQ3RDQyxnQ0FBNEIsQ0FEVTtBQUV0Q0Msd0JBQW9CLDRCQUFTQyxPQUFULEVBQWtCO0FBQ3BDLFVBQU1DLFlBQVlELFFBQVFFLGtCQUFSLEdBQTZCaEIsR0FBN0IsQ0FBaUMsVUFBQ2lCLElBQUQ7QUFBQSxlQUFVQSxLQUFLQyxPQUFMLENBQWFDLE9BQWIsQ0FBcUJKLFNBQS9CO0FBQUEsT0FBakMsRUFBMkVLLE1BQTNFLENBQWtGLFVBQUNDLEtBQUQsRUFBUUMsR0FBUjtBQUFBLGVBQWNELFFBQU1DLEdBQXBCO0FBQUEsT0FBbEYsQ0FBbEI7QUFDQSxVQUFJQyxlQUFKO0FBQ0EsVUFBSVIsWUFBWSxFQUFoQixFQUFvQjtBQUFFUSxpQkFBUyxzQkFBVDtBQUFrQyxPQUF4RCxNQUNLLElBQUlSLGFBQWEsRUFBYixJQUFtQkEsWUFBWSxHQUFuQyxFQUF3QztBQUFFUSxpQkFBUyx1QkFBVDtBQUFtQyxPQUE3RSxNQUNBO0FBQUVBLGlCQUFTLHNCQUFUO0FBQWtDOztBQUUzQyxhQUFPdEIsRUFBRXVCLE9BQUYsQ0FBVTtBQUNiQyxjQUFNLGdCQUFnQlYsU0FBaEIsR0FBNEIsZUFEckI7QUFFYlcsbUJBQVcsb0JBQW9CSDtBQUZsQixPQUFWLENBQVA7QUFJQTtBQWJzQyxHQUFyQixFQWNoQmpCLEtBZGdCLENBY1ZOLEdBZFUsQ0FBbkI7QUFlQSxNQUFJMkIsWUFBSjs7QUFFRjtBQUNBOztBQUVFQyxJQUFFQyxPQUFGLENBQVUsNEJBQVYsRUFDR0MsSUFESCxDQUNRLFVBQVNDLElBQVQsRUFBZTtBQUNuQkosbUJBQWUxQixFQUFFK0IsT0FBRixDQUFVRCxJQUFWLEVBQWU7QUFDMUJFLHFCQUFlLHVCQUFTQyxPQUFULEVBQWtCQyxLQUFsQixFQUF5QjtBQUNwQ0EsY0FBTUMsV0FBTixHQUFvQkYsUUFBUUcsVUFBUixDQUFtQkMsSUFBdkM7QUFDSCxPQUh5QixFQUFmLENBQWY7QUFJQVgsaUJBQWFyQixLQUFiLENBQW1CQyxjQUFuQjtBQUNBO0FBQ0QsR0FSSCxFQVNHdUIsSUFUSCxDQVNRO0FBQUEsV0FBSUYsRUFBRUMsT0FBRixDQUFVLDZCQUFWLENBQUo7QUFBQSxHQVRSLEVBVUdDLElBVkgsQ0FVUSxVQUFTQyxJQUFULEVBQWM7QUFDbEJBLFNBQUtRLFFBQUwsQ0FBY0MsT0FBZCxDQUFzQixVQUFTdkIsSUFBVCxFQUFlO0FBQ25DLFVBQUlrQixRQUFRUixhQUFhYyxRQUFiLENBQXNCeEIsS0FBS3lCLElBQTNCLENBQVosQ0FEbUMsQ0FDVztBQUM5QyxVQUFJQyxPQUFPMUIsS0FBS0YsU0FBTCxHQUFpQkUsS0FBSzJCLE1BQWpDO0FBQ0FULFlBQU1ELE9BQU4sQ0FBYyxTQUFkLElBQTJCakIsSUFBM0I7QUFDQWtCLFlBQU1VLFFBQU4sQ0FBZTtBQUNiQyxxQkFBYSxDQURBO0FBRWJDLG1CQUFXLGtCQUFrQjlCLEtBQUsrQixPQUF2QixHQUFpQyxHQUYvQjtBQUdiQyxlQUFPLFNBSE07QUFJYkMsZ0JBQVEsQ0FKSztBQUtiQyxpQkFBUztBQUxJLE9BQWY7O0FBUUEsVUFBSUMsZUFBZSxJQUFLQyxLQUFLQyxLQUFMLENBQVlDLFdBQVd0QyxLQUFLRixTQUFoQixJQUE2QndDLFdBQVd0QyxLQUFLMkIsTUFBaEIsQ0FBOUIsR0FBeUQsR0FBcEUsSUFBMkUsR0FBbkc7QUFDQVEscUJBQWVDLEtBQUtDLEtBQUwsQ0FBV0YsZUFBZSxHQUExQixJQUErQixHQUE5QztBQUNBLFVBQU1JLDBCQUF3QnJCLE1BQU1ELE9BQU4sQ0FBY0csVUFBZCxDQUF5Qm9CLE1BQWpELDBPQUltRHhDLEtBQUsyQixNQUp4RCx3RkFLc0QzQixLQUFLRixTQUwzRCx5RkFNdURxQyxZQU52RCx1RkFPb0RuQyxLQUFLK0IsT0FQekQseUlBQU47QUFXQWIsWUFDR3VCLFNBREgsQ0FDYUYsU0FEYixFQUVHRyxFQUZILENBRU0sV0FGTixFQUVtQixVQUFTQyxDQUFULEVBQVk7QUFDM0IsWUFBSUMsUUFBUUQsRUFBRWhCLE1BQUYsQ0FBU2tCLFFBQVQsRUFBWjtBQUNBRCxjQUFNRSxTQUFOLENBQWdCSCxFQUFFSSxNQUFsQixFQUEwQkMsTUFBMUIsQ0FBaUNqRSxHQUFqQztBQUNBNEQsVUFBRWhCLE1BQUYsQ0FBU0MsUUFBVCxDQUFrQixFQUFFSSxPQUFPLE1BQVQsRUFBbEI7QUFDRCxPQU5ILEVBT0dVLEVBUEgsQ0FPTSxVQVBOLEVBT2tCLFVBQVNDLENBQVQsRUFBWTtBQUMxQixhQUFLTSxVQUFMO0FBQ0FOLFVBQUVoQixNQUFGLENBQVNDLFFBQVQsQ0FBa0IsRUFBRUksT0FBTyxTQUFULEVBQWxCO0FBQ0QsT0FWSCxFQVdHVSxFQVhILENBV00sV0FYTixFQVdtQixVQUFTQyxDQUFULEVBQVk7QUFDM0IsWUFBSUMsUUFBUUQsRUFBRWhCLE1BQUYsQ0FBU2tCLFFBQVQsRUFBWjtBQUNBRCxjQUFNRSxTQUFOLENBQWdCSCxFQUFFSSxNQUFsQixFQUEwQkMsTUFBMUIsQ0FBaUNqRSxHQUFqQztBQUNELE9BZEg7QUFlRCxLQXhDRDtBQXlDQTtBQUNILEdBckREO0FBc0RBO0FBdERBLEdBdURDOEIsSUF2REQsQ0F1RE07QUFBQSxXQUFJRixFQUFFQyxPQUFGLENBQVUsNEJBQVYsQ0FBSjtBQUFBLEdBdkROLEVBd0RDQyxJQXhERCxDQXdETSxVQUFTcUMsWUFBVCxFQUFzQjtBQUMxQkEsaUJBQWEzQixPQUFiLENBQXFCLFVBQVN2QixJQUFULEVBQWU7QUFBQTs7QUFFbEMsVUFBSUEsS0FBS0YsU0FBTCxJQUFrQkUsS0FBSzJCLE1BQTNCLEVBQW1DO0FBQ2pDO0FBQ0Q7O0FBRUQsVUFBSVQsUUFBUVIsYUFBYWMsUUFBYixDQUFzQnhCLEtBQUt5QixJQUEzQixDQUFaO0FBQ0EsVUFBSTBCLElBQUluRSxFQUFFc0IsTUFBRixDQUFTWSxNQUFNa0MsU0FBTixFQUFULEVBQTRCO0FBQ2hDbEQsaUJBQVNGLElBRHVCO0FBRWhDcUQsY0FBTSxJQUFJckUsRUFBRXNFLE9BQU4sQ0FBYztBQUNoQjdDLHFCQUFXLGFBREs7QUFFaEJELGdCQUFNLDhHQUM2QlIsS0FBS0YsU0FEbEMsU0FDK0NFLEtBQUsyQixNQURwRDtBQUZVLFNBQWQ7QUFGMEIsT0FBNUIsQ0FBUjs7QUFTQXdCLFFBQUVWLFNBQUYsV0FBb0J6QyxLQUFLRixTQUF6QixrREFBOEVvQixNQUFNRCxPQUFOLENBQWNmLE9BQWQsQ0FBc0JxRCxJQUFwRyxrRkFDUSxFQUFDQyxRQUFRLENBQUMsQ0FBQyxDQUFGLEVBQUssQ0FBQyxFQUFOLENBQVQsRUFEUjs7QUFHQUwsUUFBRVQsRUFBRixDQUFLLFdBQUwsRUFBa0IsVUFBU0MsQ0FBVCxFQUFZO0FBQUUsYUFBS2MsU0FBTDtBQUFpQixPQUFqRCxFQUNDZixFQURELENBQ0ksVUFESixFQUNnQixVQUFDQyxDQUFELEVBQU87QUFBQyxjQUFLTSxVQUFMO0FBQWtCLE9BRDFDLEVBRUNQLEVBRkQsQ0FFSSxPQUZKLEVBRWEsVUFBQ0MsQ0FBRCxFQUFPO0FBQUVlLGVBQU9DLElBQVAsQ0FBWSxhQUFaO0FBQTJCLE9BRmpEOztBQUlBUixRQUFFaEMsV0FBRixHQUFnQixXQUFXbkIsS0FBS3lCLElBQWhDO0FBQ0E7QUFDQWhDLG1CQUFhbUUsUUFBYixDQUFzQlQsQ0FBdEI7QUFDRCxLQTFCRDtBQTJCQTtBQUNELEdBckZEOztBQXVGQTtBQUVILEMiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1hcCBmcm9tIFwiLi9tYXBcIjtcblxuY29uc29sZS5sb2coXCJMT1ZFXCIpO1xuIiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24oKSB7XG5cbiAgICBsZXQgbWFwID0gTC5tYXAoJ21hcCcpLnNldFZpZXcoWzMxLjk2ODYsIC05OS45MDE4XSwgNyk7XG4gICAgTC50aWxlTGF5ZXIoJ2h0dHBzOi8vc2VydmVyLmFyY2dpc29ubGluZS5jb20vQXJjR0lTL3Jlc3Qvc2VydmljZXMvQ2FudmFzL1dvcmxkX0xpZ2h0X0dyYXlfQmFzZS9NYXBTZXJ2ZXIvdGlsZS97en0ve3l9L3t4fScsIHtcbiAgICAgICAgYXR0cmlidXRpb246ICdUaWxlcyAmY29weTsgRXNyaSAmbWRhc2g7IEVzcmksIERlTG9ybWUsIE5BVlRFUScsXG4gICAgICAgIG1heFpvb206IDE2fSkuYWRkVG8obWFwKTtcblxuXG4gICAgdmFyIGRvb3JLbm9ja0xheWVyID0gTC5mZWF0dXJlR3JvdXAoKS5hZGRUbyhtYXApO1xuICAgIHZhciBzaGlmdExheWVyID0gTC5mZWF0dXJlR3JvdXAoKS5hZGRUbyhtYXApO1xuICAgIHZhciBzdGFnaW5nTGF5ZXIgPSBMLm1hcmtlckNsdXN0ZXJHcm91cCh7XG4gICAgICBzcGlkZXJmeURpc3RhbmNlTXVsdGlwbGllcjogNSxcbiAgICAgIGljb25DcmVhdGVGdW5jdGlvbjogZnVuY3Rpb24oY2x1c3Rlcikge1xuICAgICAgICBjb25zdCByZW1haW5pbmcgPSBjbHVzdGVyLmdldEFsbENoaWxkTWFya2VycygpLm1hcCgoaXRlbSkgPT4gaXRlbS5vcHRpb25zLnRhcmdldHMucmVtYWluaW5nKS5yZWR1Y2UoKHRvdGFsLCBudW0pPT50b3RhbCtudW0pO1xuICAgICAgICBsZXQgbWFya2VyO1xuICAgICAgICBpZiAocmVtYWluaW5nIDwgNTApIHsgbWFya2VyID0gXCJtYXJrZXItY2x1c3Rlci1zbWFsbFwiOyB9XG4gICAgICAgIGVsc2UgaWYgKHJlbWFpbmluZyA+PSA1MCAmJiByZW1haW5pbmcgPCAxMDApIHsgbWFya2VyID0gXCJtYXJrZXItY2x1c3Rlci1tZWRpdW1cIjsgfVxuICAgICAgICBlbHNlIHsgbWFya2VyID0gXCJtYXJrZXItY2x1c3Rlci1sYXJnZVwiOyB9XG5cbiAgICBcdFx0cmV0dXJuIEwuZGl2SWNvbih7XG4gICAgICAgICAgaHRtbDogJzxkaXY+PHNwYW4+JyArIHJlbWFpbmluZyArICc8L3NwYW4+PC9kaXY+JyxcbiAgICAgICAgICBjbGFzc05hbWU6IFwibWFya2VyLWNsdXN0ZXIgXCIgKyBtYXJrZXIsXG4gICAgICAgIH0pO1xuICAgIFx0fVxuICAgIH0pLmFkZFRvKG1hcCk7XG4gICAgdmFyIGdlb2pzb25MYXllcjtcblxuICAvLyBsZWFmbGV0LW1hcmtlci1pY29uIG1hcmtlci1jbHVzdGVyIG1hcmtlci1jbHVzdGVyLXNtYWxsIGxlYWZsZXQtem9vbS1hbmltYXRlZCBsZWFmbGV0LWludGVyYWN0aXZlXG4gIC8vIGxlYWZsZXQtbWFya2VyLWljb24gbWFya2VyLWNsdXN0ZXIgbWFya2VyLWNsdXN0ZXItc21hbGwgbGVhZmxldC16b29tLWFuaW1hdGVkIGxlYWZsZXQtaW50ZXJhY3RpdmVcblxuICAgICQuZ2V0SlNPTihcIi4vZGF0YS90eF9jb3VudGllcy5nZW9qc29uXCIpXG4gICAgICAudGhlbihmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgIGdlb2pzb25MYXllciA9IEwuZ2VvSnNvbihkYXRhLHtcbiAgICAgICAgICAgIG9uRWFjaEZlYXR1cmU6IGZ1bmN0aW9uKGZlYXR1cmUsIGxheWVyKSB7XG4gICAgICAgICAgICAgICAgbGF5ZXIuX2xlYWZsZXRfaWQgPSBmZWF0dXJlLnByb3BlcnRpZXMuRklQUztcbiAgICAgICAgICAgIH19KTtcbiAgICAgICAgZ2VvanNvbkxheWVyLmFkZFRvKGRvb3JLbm9ja0xheWVyKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfSlcbiAgICAgIC50aGVuKCgpPT4kLmdldEpTT04oXCIuL2RhdGEvZGVyaXZlZC90YXJnZXRzLmpzb25cIikpXG4gICAgICAudGhlbihmdW5jdGlvbihkYXRhKXtcbiAgICAgICAgZGF0YS5jb3VudGllcy5mb3JFYWNoKGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgICAgICB2YXIgbGF5ZXIgPSBnZW9qc29uTGF5ZXIuZ2V0TGF5ZXIoaXRlbS5maXBzKTsgLy95b3VyIGZlYXR1cmUgaWQgaGVyZVxuICAgICAgICAgIHZhciBjYWxjID0gaXRlbS5yZW1haW5pbmcgLyBpdGVtLnRhcmdldDtcbiAgICAgICAgICBsYXllci5mZWF0dXJlWyd0YXJnZXRzJ10gPSBpdGVtO1xuICAgICAgICAgIGxheWVyLnNldFN0eWxlKHtcbiAgICAgICAgICAgIGZpbGxPcGFjaXR5OiAxLFxuICAgICAgICAgICAgZmlsbENvbG9yOiBcInJnYmEoMjU1LDAsMCxcIiArIGl0ZW0udXJnZW5jeSArIFwiKVwiLFxuICAgICAgICAgICAgY29sb3I6IFwiI2VmZWZlZlwiLFxuICAgICAgICAgICAgd2VpZ2h0OiAyLFxuICAgICAgICAgICAgb3BhY2l0eTogMC40XG4gICAgICAgICAgfSk7XG5cbiAgICAgICAgICBsZXQgcGVyY0NvbXBsZXRlID0gMSAtIChNYXRoLnJvdW5kKChwYXJzZUZsb2F0KGl0ZW0ucmVtYWluaW5nKSAvIHBhcnNlRmxvYXQoaXRlbS50YXJnZXQpKSAqIDEwMCkgLyAxMDApO1xuICAgICAgICAgIHBlcmNDb21wbGV0ZSA9IE1hdGgucm91bmQocGVyY0NvbXBsZXRlICogMTAwKS8xMDA7XG4gICAgICAgICAgY29uc3QgcG9wdXBIVE1MID0gYDxkaXY+PGgzPiR7bGF5ZXIuZmVhdHVyZS5wcm9wZXJ0aWVzLkNPVU5UWX08L2gzPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0cj48dGg+VGFyZ2V0czwvdGg+PHRoPjwvdGg+PC90cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0cj48dGQ+VGFyZ2V0IERvb3JzPC90ZD48dGQ+JHtpdGVtLnRhcmdldH08L3RkPjwvdHI+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8dHI+PHRkPlJlbWFpbmluZyBEb29yczwvdGQ+PHRkPiR7aXRlbS5yZW1haW5pbmd9PC90ZD48L3RyPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHRyPjx0ZD5QZXJjZW50IENvbXBsZXRlPC90ZD48dGQ+JHtwZXJjQ29tcGxldGV9JTwvdGQ+PC90cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0cj48dGQ+VXJnZW5jeSBTY29yZTwvdGQ+PHRkPiR7aXRlbS51cmdlbmN5fTwvdGQ+PC90cj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3RhYmxlPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PmA7XG4gICAgICAgICAgbGF5ZXJcbiAgICAgICAgICAgIC5iaW5kUG9wdXAocG9wdXBIVE1MKVxuICAgICAgICAgICAgLm9uKCdtb3VzZW92ZXInLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAgIHZhciBwb3B1cCA9IGUudGFyZ2V0LmdldFBvcHVwKCk7XG4gICAgICAgICAgICAgIHBvcHVwLnNldExhdExuZyhlLmxhdGxuZykub3Blbk9uKG1hcCk7XG4gICAgICAgICAgICAgIGUudGFyZ2V0LnNldFN0eWxlKHsgY29sb3I6IFwiYmx1ZVwiIH0pO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5vbignbW91c2VvdXQnLCBmdW5jdGlvbihlKSB7XG4gICAgICAgICAgICAgIHRoaXMuY2xvc2VQb3B1cCgpO1xuICAgICAgICAgICAgICBlLnRhcmdldC5zZXRTdHlsZSh7IGNvbG9yOiBcIiNlZmVmZWZcIiB9KTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAub24oJ21vdXNlbW92ZScsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgICAgdmFyIHBvcHVwID0gZS50YXJnZXQuZ2V0UG9wdXAoKTtcbiAgICAgICAgICAgICAgcG9wdXAuc2V0TGF0TG5nKGUubGF0bG5nKS5vcGVuT24obWFwKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgIH0pXG4gICAgLy9DbHVzdGVyaW5nIG9mIHN0YWdpbmdcbiAgICAudGhlbigoKT0+JC5nZXRKU09OKFwiLi9kYXRhL3NhbXBsZS1zdGFnaW5nLmpzb25cIikpXG4gICAgLnRoZW4oZnVuY3Rpb24oc3RhZ2luZ1NpdGVzKXtcbiAgICAgIHN0YWdpbmdTaXRlcy5mb3JFYWNoKGZ1bmN0aW9uKGl0ZW0pIHtcblxuICAgICAgICBpZiAoaXRlbS5yZW1haW5pbmcgPT0gaXRlbS50YXJnZXQpIHtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgbGF5ZXIgPSBnZW9qc29uTGF5ZXIuZ2V0TGF5ZXIoaXRlbS5maXBzKTtcbiAgICAgICAgdmFyIG0gPSBMLm1hcmtlcihsYXllci5nZXRDZW50ZXIoKSwge1xuICAgICAgICAgICAgdGFyZ2V0czogaXRlbSxcbiAgICAgICAgICAgIGljb246IG5ldyBMLkRpdkljb24oe1xuICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ215LWRpdi1pY29uJyxcbiAgICAgICAgICAgICAgICBodG1sOiAnPGRpdiBjbGFzcz1cImljb24tY29udGFpbmVyXCI+PGltZyBjbGFzcz1cIm15LWRpdi1pbWFnZVwiIHNyYz1cIi9pbWcvbWFya2VyXCIvPicrXG4gICAgICAgICAgICAgICAgICAgICAgYDxzcGFuIGNsYXNzPVwibXktZGl2LXNwYW5cIj4ke2l0ZW0ucmVtYWluaW5nfS8ke2l0ZW0udGFyZ2V0fTwvc3Bhbj48L2Rpdj5gXG4gICAgICAgICAgICB9KVxuICAgICAgICB9KTtcblxuICAgICAgICBtLmJpbmRQb3B1cChgPGRpdj4ke2l0ZW0ucmVtYWluaW5nfSBWb2x1bnRlZXIgSFEncyBuZWVkZWQgdG8gc2V0IDxici8+dXAgaW4gJHtsYXllci5mZWF0dXJlLnRhcmdldHMubmFtZX0gQ291bnR5PC9kaXY+PGJyLz48ZGl2PjxzdHJvbmc+Q2xpY2sgdG8gc3RhcnQgYSBzdGFnaW5nIHNpdGU8L3N0cm9uZz48L2Rpdj5gLFxuICAgICAgICAgICAgICAgIHtvZmZzZXQ6IFstNCwgLTIwXX0pO1xuXG4gICAgICAgIG0ub24oJ21vdXNlb3ZlcicsIGZ1bmN0aW9uKGUpIHsgdGhpcy5vcGVuUG9wdXAoKX0pXG4gICAgICAgIC5vbignbW91c2VvdXQnLCAoZSkgPT4ge3RoaXMuY2xvc2VQb3B1cCgpfSlcbiAgICAgICAgLm9uKCdjbGljaycsIChlKSA9PiB7IHdpbmRvdy5vcGVuKCcvc3RhcnQuaHRtbCcpfSlcblxuICAgICAgICBtLl9sZWFmbGV0X2lkID0gXCJtYXJrZXJcIiArIGl0ZW0uZmlwcztcbiAgICAgICAgLy8gbS5hZGRUbyhzdGFnaW5nTGF5ZXIpO1xuICAgICAgICBzdGFnaW5nTGF5ZXIuYWRkTGF5ZXIobSlcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuO1xuICAgIH0pO1xuXG4gICAgLy8gVGFiIGV2ZW50c1xuXG59XG4iXX0=
